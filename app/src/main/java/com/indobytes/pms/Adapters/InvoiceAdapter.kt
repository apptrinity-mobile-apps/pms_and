package com.indobytes.pms.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.indobytes.pms.ApiPojo.InvoiceDetailsResponse
import com.indobytes.pms.InvoiceDetailsActivity
import com.indobytes.pms.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


@SuppressLint("SetTextI18n")
class InvoiceAdapter(context: Context, list: ArrayList<InvoiceDetailsResponse>) :
    RecyclerView.Adapter<InvoiceAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var previousExpandedPosition = -1
    private var mExpandedPosition = -1
    private var mList: ArrayList<InvoiceDetailsResponse> = ArrayList()

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_recycle_invoice, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return mList.size

    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        if (mList[position].invoice_id.equals(null) || mList[position].invoice_id!! == "") {
            holder.tv_invoice_number.text = "-"
        } else {
            holder.tv_invoice_number.text = "# ${mList[position].invoice_id!!}"
        }

        if (mList[position].total_price.equals(null) || mList[position].total_price!! == "") {
            holder.tv_invoice_amount.text = "-"
        } else {
            val price = mList[position].total_price
            val checkDecimal = price!!.contains(".")
            if (checkDecimal) {
                val unit_price = String.format("%.2f", price.toFloat())
                holder.tv_invoice_amount.text = "$$unit_price"
            } else {
                holder.tv_invoice_amount.text = "$$price"
            }
        }

        if (mList[position].customer_name.equals(null) || mList[position].customer_name!! == "") {
            holder.tv_invoice_cust_name.text = "-"
        } else {
            holder.tv_invoice_cust_name.text = mList[position].customer_name!!
        }

        if (mList[position].invoice_date.equals(null) || mList[position].invoice_date!! == "") {
            holder.tv_invoice_date.text = "-"
        } else {

            holder.tv_invoice_date.text = editDateFormat(mList[position].invoice_date!!)
        }

        if (mList[position].due_date.equals(null) || mList[position].due_date!! == "") {
            holder.tv_invoice_details_date.text = "-"
        } else {
            holder.tv_invoice_details_date.text = editDateFormat(mList[position].due_date!!)
        }

        if (mList[position].sales_comm_per == "") {
            holder.tv_invoice_details_sales_comm_percent.text =
                mContext!!.resources.getString(R.string.sales_comm_percent) + " (-%)"
        } else {
            holder.tv_invoice_details_sales_comm_percent.text =
                mContext!!.resources.getString(R.string.sales_comm_percent) + " (" +
                        mList[position].sales_comm_per + "%)"
        }

        if (mList[position].sales_split_per == "") {
            holder.tv_invoice_details_sales_split_percent.text =
                mContext!!.resources.getString(R.string.sales_split_percent) + " (-%)"
        } else {
            holder.tv_invoice_details_sales_split_percent.text =
                mContext!!.resources.getString(R.string.sales_split_percent) + " (" + mList[position].sales_split_per + "%)"
        }

        if (mList[position].sales_split_amount == "") {
            holder.tv_invoice_details_sales_split_amount.text = "-"
        } else {

            val sales_split_amount = mList[position].sales_split_amount
            if (sales_split_amount!!.contains(".")) {
                holder. tv_invoice_details_sales_split_amount.text = stringFormat(sales_split_amount)
            } else {
                holder. tv_invoice_details_sales_split_amount.text = sales_split_amount
            }

            //holder.tv_invoice_details_sales_split_amount.text = mList[position].sales_split_amount
        }

        if (mList[position].sales_comm_amount == "") {
            holder.tv_invoice_details_sales_comm_amount.text = "-"
        } else {

            val sales_comm_amount = mList[position].sales_comm_amount
            if (sales_comm_amount!!.contains(".")) {
                holder.tv_invoice_details_sales_comm_amount.text = stringFormat(sales_comm_amount)
            } else {
                holder.tv_invoice_details_sales_comm_amount.text = sales_comm_amount
            }

          //  holder.tv_invoice_details_sales_comm_amount.text = mList[position].sales_comm_amount
        }

        holder.tv_invoice_details_qty.text = "-"
        holder.tv_invoice_details_unit_price.text = "-"
        if (mList[position].project_name.equals(null) || mList[position].project_name == "") {
            holder.tv_invoice_details_project_name.text = "-"
        } else {
            holder.tv_invoice_details_project_name.text = mList[position].project_name!!
        }

        if (mList[position].sales_comm_total.equals(null)) {
            holder.tv_invoice_details_amount.text = "-"
        } else {
            holder.tv_invoice_details_amount.text = mList[position].sales_comm_total!!
        }

        if (mList[position].status.equals(null) || mList[position].status!! == "") {
            holder.tv_invoice_details_status.text = "-"
        } else {
            when (mList[position].status!!) {
                "0" -> {
                    holder.tv_invoice_details_status.text = "Due"
                }
                "1" -> {
                    holder.tv_invoice_details_status.text = "Paid"
                }
                "2" -> {
                    holder.tv_invoice_details_status.text = "Paid"
                }
            }
        }

        val isExpanded = position == mExpandedPosition
        holder.ll_invoice_details.visibility = if (isExpanded)
            View.VISIBLE
        else
            View.GONE

        if (isExpanded) {
            holder.iv_drop_down.setImageResource((R.drawable.ic_arrow_up))
            holder.ll_invoice_item.setBackgroundResource(R.drawable.item_background_selected)
            holder.ll_invoice_header.setBackgroundResource(R.drawable.item_background_selected_header)
            holder.ll_invoice_details.requestFocus()

            // setting text color based on android version

            holder.tv_invoice_number.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.item_text_color_selected)
            )
            holder.tv_invoice_amount.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.item_text_color_selected)
            )
            holder.tv_invoice_cust_name.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.item_text_color_selected)
            )
            holder.tv_invoice_date.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.item_text_color_selected)
            )

        } else {
            holder.iv_drop_down.setImageResource((R.drawable.ic_arrow_down))
            holder.ll_invoice_details.requestFocus()
            holder.ll_invoice_item.setBackgroundResource(R.drawable.item_background_default)
            holder.ll_invoice_header.setBackgroundResource(R.drawable.item_background_default)

            // setting text color based on android version

            holder.tv_invoice_number.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.item_text_color_default)
            )
            holder.tv_invoice_amount.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.item_text_color_default)
            )
            holder.tv_invoice_cust_name.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.item_text_color_default)
            )
            holder.tv_invoice_date.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.item_text_color_default)
            )

        }

        holder.ll_invoice_header.isActivated = isExpanded

        if (isExpanded)
            previousExpandedPosition = position

        holder.ll_invoice_header.setOnClickListener {

            mExpandedPosition = if (isExpanded) -1 else position
            notifyItemChanged(previousExpandedPosition)
            notifyItemChanged(position)

        }

        holder.tv_view.setOnClickListener {
            val intent = Intent(mContext, InvoiceDetailsActivity::class.java)
            intent.putExtra("invoice_id", mList[position].invoice_id!!)
            mContext!!.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_view: TextView = view.findViewById(R.id.tv_view)
        var tv_invoice_cust_name: TextView = view.findViewById(R.id.tv_invoice_cust_name)
        var tv_invoice_date: TextView = view.findViewById(R.id.tv_invoice_date)
        var tv_invoice_amount: TextView = view.findViewById(R.id.tv_invoice_amount)
        var tv_invoice_number: TextView = view.findViewById(R.id.tv_invoice_number)
        var tv_invoice_details_qty: TextView = view.findViewById(R.id.tv_invoice_details_qty)
        var tv_invoice_details_sales_split_percent: TextView =
            view.findViewById(R.id.tv_invoice_details_sales_split_percent)
        var tv_invoice_details_sales_comm_percent: TextView =
            view.findViewById(R.id.tv_invoice_details_sales_comm_percent)
        var tv_invoice_details_sales_split_amount: TextView =
            view.findViewById(R.id.tv_invoice_details_sales_split_amount)
        var tv_invoice_details_sales_comm_amount: TextView =
            view.findViewById(R.id.tv_invoice_details_sales_comm_amount)
        var tv_invoice_details_date: TextView = view.findViewById(R.id.tv_invoice_details_date)
        var tv_invoice_details_unit_price: TextView =
            view.findViewById(R.id.tv_invoice_details_unit_price)
        var tv_invoice_details_project_name: TextView =
            view.findViewById(R.id.tv_invoice_details_project_name)
        var tv_invoice_details_amount: TextView = view.findViewById(R.id.tv_invoice_details_amount)
        var tv_invoice_details_status: TextView = view.findViewById(R.id.tv_invoice_details_status)

        var iv_drop_down: ImageView = view.findViewById(R.id.iv_drop_down)
        var ll_invoice_item: LinearLayout = view.findViewById(R.id.ll_invoice_item)
        var ll_invoice_header: LinearLayout = view.findViewById(R.id.ll_invoice_header)
        var ll_invoice_details: LinearLayout = view.findViewById(R.id.ll_invoice_details)

    }


    private fun editDateFormat(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d("RegisterAccount", "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }

    private fun stringFormat(value: String): String {
        return String.format("%.2f", value.toFloat())
    }

}
    
