package com.indobytes.pms.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.indobytes.pms.ApiPojo.InvoiceDetailsResponse
import com.indobytes.pms.ApiPojo.PaidInvoicesDataResponse
import com.indobytes.pms.InvoiceDetailsActivity
import com.indobytes.pms.PaidInvoiceActivity
import com.indobytes.pms.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


@SuppressLint("SetTextI18n")
class InvoicePaidAdapter(context: Context, list: ArrayList<PaidInvoicesDataResponse>) :
    RecyclerView.Adapter<InvoicePaidAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<PaidInvoicesDataResponse> = ArrayList()

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_recycle_paid_invoice, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return mList.size

    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        if (mList[position].transaction_id.equals(null) || mList[position].transaction_id == "") {
            holder.tv_paid_transaction_no.text = "-"
        } else {
            holder.tv_paid_transaction_no.text = "# ${mList[position].transaction_id}"
        }
        if (mList[position].payment_type.equals(null) || mList[position].payment_type == "") {
            holder.tv_paid_invoice_payment_type.text = "-"
        } else {
            holder.tv_paid_invoice_payment_type.text = mList[position].payment_type
        }
        if (mList[position].total_amount.equals(null) || mList[position].total_amount == "") {
            holder.tv_paid_invoice_comm_amount.text = "-"
        } else {
            holder.tv_paid_invoice_comm_amount.text = "$ ${mList[position].total_amount}"
        }
        if (mList[position].transfer_on.equals(null) || mList[position].transfer_on == "") {
            holder.tv_paid_invoice_date.text = "-"
        } else {
            holder.tv_paid_invoice_date.text = editDateFormat(mList[position].transfer_on!!)
        }

        holder.ll_paid_invoice_header.setOnClickListener {
            val intent = Intent(mContext!!, PaidInvoiceActivity::class.java)
            if (mList[position].transaction_id.equals(null) || mList[position].transaction_id == "") {
                intent.putExtra("transaction_id", "-")
            } else {
                intent.putExtra("transaction_id", mList[position].transaction_id)
            }
            if (mList[position].total_amount.equals(null) || mList[position].total_amount == "") {
                intent.putExtra("total_amount", "-")
            } else {
                intent.putExtra("total_amount", mList[position].total_amount)
            }
            if (mList[position].payment_type.equals(null) || mList[position].payment_type == "") {
                intent.putExtra("payment_type", "-")
            } else {
                intent.putExtra("payment_type", mList[position].payment_type)
            }
            if (mList[position].bank_name.equals(null) || mList[position].bank_name == "") {
                intent.putExtra("bank_name", "-")
            } else {
                intent.putExtra("bank_name", mList[position].bank_name)
            }
            if (mList[position].cheque_number.equals(null) || mList[position].cheque_number == "") {
                intent.putExtra("cheque_number", "-")
            } else {
                intent.putExtra("cheque_number", mList[position].cheque_number)
            }
            if (mList[position].acholder_name.equals(null) || mList[position].acholder_name == "") {
                intent.putExtra("acholder_name", "-")
            } else {
                intent.putExtra("acholder_name", mList[position].acholder_name)
            }
            if (mList[position].ac_number.equals(null) || mList[position].ac_number == "") {
                intent.putExtra("ac_number", "-")
            } else {
                intent.putExtra("ac_number", mList[position].ac_number)
            }
            if (mList[position].branch_name.equals(null) || mList[position].branch_name == "") {
                intent.putExtra("branch_name", "-")
            } else {
                intent.putExtra("branch_name", mList[position].branch_name)
            }
            if (mList[position].ifsc_code.equals(null) || mList[position].ifsc_code == "") {
                intent.putExtra("ifsc_code", "-")
            } else {
                intent.putExtra("ifsc_code", mList[position].ifsc_code)
            }
            if (mList[position].transfer_on.equals(null) || mList[position].transfer_on == "") {
                intent.putExtra("transfer_on", "-")
            } else {
                intent.putExtra("transfer_on", editDateFormat(mList[position].transfer_on!!))
            }
            intent.putExtra("invoice_list", mList[position].invoicelist)
            mContext!!.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_paid_transaction_no: TextView = view.findViewById(R.id.tv_paid_transaction_no)
        var tv_paid_invoice_comm_amount: TextView =
            view.findViewById(R.id.tv_paid_invoice_comm_amount)
        var tv_paid_invoice_payment_type: TextView =
            view.findViewById(R.id.tv_paid_invoice_payment_type)
        var tv_paid_invoice_date: TextView = view.findViewById(R.id.tv_paid_invoice_date)

        var ll_paid_invoice_header: LinearLayout = view.findViewById(R.id.ll_paid_invoice_header)

    }


    private fun editDateFormat(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d("RegisterAccount", "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }

}
    
