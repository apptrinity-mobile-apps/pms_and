package com.indobytes.pms.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.indobytes.pms.R


class NavigationAdapter(context: Context, title: Array<String>, icon: Array<Int>) :
    RecyclerView.Adapter<NavigationAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mTitle: Array<String>? = null
    private var mIcon: Array<Int>? = null
    private var selected = -1

    init {
        this.mContext = context
        this.mTitle = title
        this.mIcon = icon
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_recycle_navigation, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return mTitle!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.tv_item_nav_header_text.text = mTitle!![position]
        holder.iv_item_nav_next.setImageResource(mIcon!![position])
        Log.d("selected", "=== $selected")
        if (selected == -1) {
            holder.tv_item_nav_header_text.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.nav_text_color)
            )

            holder.iv_item_nav_next.setImageResource(R.drawable.ic_arrow_next)
        } else {
            holder.tv_item_nav_header_text.setTextColor(
                ContextCompat.getColor(mContext!!, R.color.nav_text_color_selected)
            )

            holder.iv_item_nav_next.setImageResource(R.drawable.ic_arrow_next_selected)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_item_nav_header_text: TextView = view.findViewById(R.id.tv_item_nav_header_text)
        var iv_item_nav_next: ImageView = view.findViewById(R.id.iv_item_nav_next)
    }

    fun getItem(position: Int): String {
        return mTitle!![position]
    }

    fun setSelected(position: Int) {
        selected = position
    }
}
    
