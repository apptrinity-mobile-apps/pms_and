package com.indobytes.pms.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.indobytes.pms.ApiPojo.InvoiceItemsDataResponse
import com.indobytes.pms.R
import java.text.SimpleDateFormat
import java.util.*
import java.util.Collections.replaceAll
import kotlin.collections.ArrayList


@SuppressLint("SetTextI18n")
class InvoiceItemDetailsAdapter(context: Context, list: ArrayList<InvoiceItemsDataResponse>) :
    RecyclerView.Adapter<InvoiceItemDetailsAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<InvoiceItemsDataResponse> = ArrayList()

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_module_project_details, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return mList.size

    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        if (mList[position].service_description == "") {
            holder.tv_project_details_shipping_method.text = "-"
        } else {
            holder.tv_project_details_shipping_method.text = mList[position].service_description +"\n"+"("+mList[position].line_item_code_text+"("+mList[position].line_item_code+")"+")"
        }
        if (mList[position].qty == "") {
            holder.tv_project_details_qty.text = mList[position].qty
        } else {
            holder.tv_project_details_qty.text = mList[position].qty
        }
        if (mList[position].unit_price == "") {
            holder.tv_project_details_unit_price.text = "-"
        } else {
            val price = mList[position].unit_price
            val unit_price = String.format("%.2f", price!!.toFloat())
            holder.tv_project_details_unit_price.text = unit_price
        }
        if (mList[position].item_total_price == "") {
            holder.tv_project_details_sub_total.text = "-"
        } else {
            val price = mList[position].item_total_price
            val unit_price = String.format("%.2f", price!!.toFloat())
            holder.tv_project_details_sub_total.text = unit_price
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_project_details_shipping_method: TextView =
            view.findViewById(R.id.tv_project_details_shipping_method)
        var tv_project_details_qty: TextView = view.findViewById(R.id.tv_project_details_qty)
        var tv_project_details_unit_price: TextView =
            view.findViewById(R.id.tv_project_details_unit_price)
        var tv_project_details_sub_total: TextView =
            view.findViewById(R.id.tv_project_details_sub_total)

    }


    private fun editDateFormat(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d("RegisterAccount", "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }

}
    
