package com.indobytes.pms

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.indobytes.pms.Adapters.InvoiceItemDetailsAdapter
import com.indobytes.pms.ApiPojo.*
import com.indobytes.pms.Helper.SessionManager
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@SuppressLint("SetTextI18n")
class InvoiceDetailsActivity : AppCompatActivity() {

    private lateinit var invoiceToolBar: Toolbar
    private lateinit var iv_invoice_nav_menu: ImageView

    private lateinit var ll_module_customer: LinearLayout
    private lateinit var ll_customer_header: LinearLayout
    private lateinit var ll_customer: LinearLayout
    private lateinit var iv_customer_drop_down: ImageView
    private lateinit var tv_invoice_number: TextView
    private lateinit var tv_invoice_amount_total: TextView
    private lateinit var tv_invoice_module_po_no: TextView
    private lateinit var tv_invoice_module_due: TextView
    private lateinit var tv_invoice_module_custaddress: TextView
    private lateinit var tv_invoice_module_invoicedate: TextView
    private lateinit var tv_invoice_module_cust_name: TextView
    private lateinit var tv_comm_module_commissionable_amount: TextView
    private lateinit var tv_invoice_module_cust_id: TextView
    private lateinit var tv_invoice_module_fax: TextView
    private lateinit var tv_invoice_module_mobile_no: TextView

    private lateinit var ll_module_shipping: LinearLayout
    private lateinit var ll_shipping_header: LinearLayout
    private lateinit var ll_shipping: LinearLayout
    private lateinit var iv_shipping_drop_down: ImageView
    private lateinit var tv_shipping_module_header: TextView
    private lateinit var tv_shipping_module_shipping_contact: TextView
    private lateinit var tv_shipping_module_mobile_no: TextView
    private lateinit var tv_shipping_module_date: TextView
    private lateinit var tv_shipping_module_address: TextView
    private lateinit var tv_shipping_module_zip_code: TextView

    private lateinit var ll_module_project_details: LinearLayout
    private lateinit var ll_project_details_header: LinearLayout
    private lateinit var ll_project_details: LinearLayout
    private lateinit var iv_project_drop_down: ImageView
    private lateinit var rv_invoice_details: RecyclerView

    private lateinit var ll_module_commissions: LinearLayout
    private lateinit var ll_commissions: LinearLayout
    private lateinit var ll_commissions_header: LinearLayout
    private lateinit var iv_commissions_drop_down: ImageView
    private lateinit var tv_comm_module_comm_percent: TextView
    private lateinit var tv_comm_module_comm_amount: TextView
    private lateinit var tv_comm_module_plus_amount: TextView
    private lateinit var tv_comm_module_sales_percent: TextView
    private lateinit var tv_comm_module_sales_amount: TextView
    private lateinit var tv_comm_module_sales_total: TextView

    private lateinit var mDialog: Dialog
    private lateinit var sessionManager: SessionManager
    private lateinit var userDetails: HashMap<String, String>
    private lateinit var invoiceDetailsList: ArrayList<InvoiceItemsDataResponse>
    private lateinit var invoiceArrayList: ArrayList<InvoiceDetailsResponse>
    private lateinit var invoiceDetailsAdapter: InvoiceItemDetailsAdapter

    private val activity_name = "InvoiceDetailsActivity"


    private var isCustomerExpanded = true
    private var isShippingExpanded = false
    private var isProjectExpanded = false
    private var isCommissionsExpanded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invoice_detailed_view)

        sessionManager = SessionManager(this)
        userDetails = sessionManager.userDetails
        init()

        val invoice_id = intent.getStringExtra("invoice_id")

        itemDetails(invoice_id, userDetails[SessionManager.EXECUTIVE_ID]!!)

        filterList(invoice_id)

        iv_invoice_nav_menu.setOnClickListener {
            onBackPressed()
        }


        ll_customer_header.setOnClickListener {
            if (isCustomerExpanded) {
                iv_customer_drop_down.setImageResource((R.drawable.ic_arrow_down))
                ll_customer.visibility = View.GONE
                ll_customer_header.setBackgroundResource(R.drawable.item_background_default_header)
                ll_module_customer.setBackgroundResource(R.drawable.item_background_default)
                isCustomerExpanded = false
            } else {
                iv_customer_drop_down.setImageResource((R.drawable.ic_arrow_up))
                ll_customer.visibility = View.VISIBLE
                ll_customer_header.setBackgroundResource(R.drawable.item_background_selected_header)
                ll_module_customer.setBackgroundResource(R.drawable.item_background_selected)
                isCustomerExpanded = true
            }
        }

        ll_shipping_header.setOnClickListener {
            if (isShippingExpanded) {
                iv_shipping_drop_down.setImageResource((R.drawable.ic_arrow_down))
                ll_shipping.visibility = View.GONE
                ll_shipping_header.setBackgroundResource(R.drawable.item_background_default_header)
                ll_module_shipping.setBackgroundResource(R.drawable.item_background_default)
                isShippingExpanded = false
            } else {
                iv_shipping_drop_down.setImageResource((R.drawable.ic_arrow_up))
                ll_shipping.visibility = View.VISIBLE
                ll_shipping_header.setBackgroundResource(R.drawable.item_background_selected_header)
                ll_module_shipping.setBackgroundResource(R.drawable.item_background_selected)
                isShippingExpanded = true
            }
        }

        ll_project_details_header.setOnClickListener {
            if (isProjectExpanded) {
                iv_project_drop_down.setImageResource((R.drawable.ic_arrow_down))
                ll_project_details.visibility = View.GONE
                ll_project_details_header.setBackgroundResource(R.drawable.item_background_default_header)
                ll_module_project_details.setBackgroundResource(R.drawable.item_background_default)
                isProjectExpanded = false
            } else {
                iv_project_drop_down.setImageResource((R.drawable.ic_arrow_up))
                ll_project_details.visibility = View.VISIBLE
                ll_project_details_header.setBackgroundResource(R.drawable.item_background_selected_header)
                ll_module_project_details.setBackgroundResource(R.drawable.item_background_selected)
                isProjectExpanded = true
            }
        }

        ll_commissions_header.setOnClickListener {
            if (isCommissionsExpanded) {
                iv_commissions_drop_down.setImageResource((R.drawable.ic_arrow_down))
                ll_commissions.visibility = View.GONE
                ll_commissions_header.setBackgroundResource(R.drawable.item_background_default_header)
                ll_module_commissions.setBackgroundResource(R.drawable.item_background_default)
                isCommissionsExpanded = false
            } else {
                iv_commissions_drop_down.setImageResource((R.drawable.ic_arrow_up))
                ll_commissions.visibility = View.VISIBLE
                ll_commissions_header.setBackgroundResource(R.drawable.item_background_selected_header)
                ll_module_commissions.setBackgroundResource(R.drawable.item_background_selected)
                isCommissionsExpanded = true
            }
        }

    }

    private fun init() {
        invoiceToolBar = findViewById(R.id.invoiceToolBar)
        iv_invoice_nav_menu = invoiceToolBar.findViewById(R.id.iv_invoice_nav_menu)

        ll_module_customer = findViewById(R.id.ll_module_customer)
        ll_customer = findViewById(R.id.ll_customer)
        ll_customer_header = findViewById(R.id.ll_customer_header)
        iv_customer_drop_down = findViewById(R.id.iv_customer_drop_down)
        tv_invoice_number = findViewById(R.id.tv_invoice_number)
        tv_invoice_amount_total = findViewById(R.id.tv_invoice_amount_total)
        tv_invoice_module_po_no = findViewById(R.id.tv_invoice_module_po_no)
        tv_invoice_module_due = findViewById(R.id.tv_invoice_module_due)
        tv_invoice_module_custaddress = findViewById(R.id.tv_invoice_module_custaddress)
        tv_invoice_module_invoicedate = findViewById(R.id.tv_invoice_module_invoicedate)
        tv_invoice_module_cust_name = findViewById(R.id.tv_invoice_module_cust_name)
        tv_comm_module_commissionable_amount = findViewById(R.id.tv_comm_module_commissionable_amount)
        tv_invoice_module_cust_id = findViewById(R.id.tv_invoice_module_cust_id)
        tv_invoice_module_fax = findViewById(R.id.tv_invoice_module_fax)
        tv_invoice_module_mobile_no = findViewById(R.id.tv_invoice_module_mobile_no)

        ll_module_shipping = findViewById(R.id.ll_module_shipping)
        ll_shipping = findViewById(R.id.ll_shipping)
        ll_shipping_header = findViewById(R.id.ll_shipping_header)
        iv_shipping_drop_down = findViewById(R.id.iv_shipping_drop_down)
        tv_shipping_module_header = findViewById(R.id.tv_shipping_module_header)
        tv_shipping_module_shipping_contact = findViewById(R.id.tv_shipping_module_shipping_contact)
        tv_shipping_module_mobile_no = findViewById(R.id.tv_shipping_module_mobile_no)
        tv_shipping_module_date = findViewById(R.id.tv_shipping_module_date)
        tv_shipping_module_address = findViewById(R.id.tv_shipping_module_address)
        tv_shipping_module_zip_code = findViewById(R.id.tv_shipping_module_zip_code)

        ll_module_project_details = findViewById(R.id.ll_module_project_details)
        ll_project_details = findViewById(R.id.ll_project_details)
        ll_project_details_header = findViewById(R.id.ll_project_details_header)
        iv_project_drop_down = findViewById(R.id.iv_project_drop_down)
        rv_invoice_details = findViewById(R.id.rv_invoice_details)

        ll_module_commissions = findViewById(R.id.ll_module_commissions)
        ll_commissions = findViewById(R.id.ll_commissions)
        ll_commissions_header = findViewById(R.id.ll_commissions_header)
        iv_commissions_drop_down = findViewById(R.id.iv_commissions_drop_down)
        tv_comm_module_comm_percent = findViewById(R.id.tv_comm_module_comm_percent)
        tv_comm_module_comm_amount = findViewById(R.id.tv_comm_module_comm_amount)
        tv_comm_module_plus_amount = findViewById(R.id.tv_comm_module_plus_amount)
        tv_comm_module_sales_percent = findViewById(R.id.tv_comm_module_sales_percent)
        tv_comm_module_sales_amount = findViewById(R.id.tv_comm_module_sales_amount)
        tv_comm_module_sales_total = findViewById(R.id.tv_comm_module_sales_total)

        val layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        rv_invoice_details.layoutManager = layoutManager
        rv_invoice_details.setHasFixedSize(true)

        mDialog = Dialog(this)
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog.setContentView(R.layout.alert_dialog_loading)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog.setCancelable(false)
        mDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    mDialog.dismiss()
                }
                return true
            }
        })

        invoiceDetailsList = ArrayList()


    }

    private fun itemDetails(invoice_id: String, user_id: String) {
        mDialog.show()
        invoiceDetailsList = ArrayList()

        val invoiceItems = ApiInterface.create()
        val call = invoiceItems.invoiceItemsApi(invoice_id, user_id)
        call.enqueue(object : Callback<InvoiceItemsResponse> {
            override fun onFailure(call: Call<InvoiceItemsResponse>, t: Throwable) {
                Log.d(activity_name, "invoiceItemsApi error $t")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(this@InvoiceDetailsActivity, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<InvoiceItemsResponse>, response: Response<InvoiceItemsResponse>
            ) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        val dataList = response.body()!!.invoice_items
                        invoiceDetailsList = dataList!!
                        invoiceDetailsAdapter = InvoiceItemDetailsAdapter(
                            this@InvoiceDetailsActivity,
                            invoiceDetailsList
                        )
                        rv_invoice_details.adapter = invoiceDetailsAdapter
                    } else {
                        Toast.makeText(
                            this@InvoiceDetailsActivity,
                            response.body()!!.result,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

    private fun filterList(invoice_id: String) {
        mDialog.show()
        invoiceArrayList = ArrayList()
        val invoiceFilterList = ApiInterface.create()
        val call = invoiceFilterList.viewInvoiceApi(
            invoice_id,
            "",
            userDetails[SessionManager.EXECUTIVE_ID]!!
        )
        call.enqueue(object : Callback<InvoiceResponse> {
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                Log.d(activity_name, "viewInvoiceApi error $t")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(this@InvoiceDetailsActivity, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>, response: Response<InvoiceResponse>
            ) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        val invoice_list = response.body()!!.invoice_details
                        invoiceArrayList = invoice_list!!

                        for (i in 0 until invoiceArrayList.size) {
                            // customer details module
                            if (invoiceArrayList[i].invoice_id == "") {
                                tv_invoice_number.text = "-"
                            } else {
                                tv_invoice_number.text = "# ${invoiceArrayList[i].invoice_id}"
                            }
                            if (invoiceArrayList[i].total_price == "") {
                                tv_invoice_amount_total.text = "-"
                            } else {
                                val price = invoiceArrayList[i].total_price
                                val checkDecimal = price!!.contains(".")
                                if (checkDecimal) {
                                    tv_invoice_amount_total.text = "$" + stringFormat(price)
                                } else {
                                    tv_invoice_amount_total.text = "$" + price
                                }
                            }
                            if (invoiceArrayList[i].po_number == "") {
                                tv_invoice_module_po_no.text = "-"
                            } else {
                                tv_invoice_module_po_no.text = invoiceArrayList[i].po_number
                            }
                            if (invoiceArrayList[i].due_date == "") {
                                tv_invoice_module_due.text = "-"
                            } else {
                                tv_invoice_module_due.text =
                                    editDateFormat(invoiceArrayList[i].due_date!!)
                            }
                            if (invoiceArrayList[i].customer_addresslane1 == "") {
                                tv_invoice_module_custaddress.text = "-"
                            } else {
                                tv_invoice_module_custaddress.text =
                                    invoiceArrayList[i].customer_addresslane1!!+" "+invoiceArrayList[i].customer_addresslane2!!+" "+invoiceArrayList[i].customer_zipcode!!
                            }
                            if (invoiceArrayList[i].invoice_date == "") {
                                tv_invoice_module_invoicedate.text = "-"
                            } else {
                                tv_invoice_module_invoicedate.text =
                                    editDateFormat(invoiceArrayList[i].invoice_date!!)
                            }
                            if (invoiceArrayList[i].customer_name == "") {
                                tv_invoice_module_cust_name.text = ""
                            } else {
                                tv_invoice_module_cust_name.text = invoiceArrayList[i].customer_name
                            }
                            if (invoiceArrayList[i].remaining_total_amount == "") {
                                tv_comm_module_commissionable_amount.text = ""
                            } else {
                                tv_comm_module_commissionable_amount.text = invoiceArrayList[i].remaining_total_amount
                            }
                            if (invoiceArrayList[i].customer_id == "") {
                                tv_invoice_module_cust_id.text = ""
                            } else {
                                tv_invoice_module_cust_id.text = invoiceArrayList[i].customer_id
                            }
                            if (invoiceArrayList[i].customer_fax == "") {
                                tv_invoice_module_fax.text = "-"
                            } else {
                                tv_invoice_module_fax.text = invoiceArrayList[i].customer_fax
                            }
                            if (invoiceArrayList[i].customer_phone == "") {
                                tv_invoice_module_mobile_no.text = "-"
                            } else {
                                tv_invoice_module_mobile_no.text =
                                    invoiceArrayList[i].customer_phone
                            }

                            // shipping module
                            if (invoiceArrayList[i].project_name == "") {
                                tv_shipping_module_header.text = "-"
                            } else {
                                tv_shipping_module_header.text = invoiceArrayList[i].project_name
                            }
                            if (invoiceArrayList[i].ship_contact == "") {
                                tv_shipping_module_shipping_contact.text = "-"
                            } else {
                                tv_shipping_module_shipping_contact.text =
                                    invoiceArrayList[i].ship_contact
                            }
                            if (invoiceArrayList[i].ship_telephone == "") {
                                tv_shipping_module_mobile_no.text = "-"
                            } else {
                                tv_shipping_module_mobile_no.text =
                                    invoiceArrayList[i].ship_telephone
                            }
                            tv_shipping_module_date.text = "-"
                            val address =
                                invoiceArrayList[i].ship_addresslani1 + " " + invoiceArrayList[i].ship_addresslane2 + " " + invoiceArrayList[i].ship_city + " " + invoiceArrayList[i].ship_state
                            if (address == "") {
                                tv_shipping_module_address.text = "-"
                            } else {
                                tv_shipping_module_address.text = address
                            }
                            if (invoiceArrayList[i].ship_zipcode == "") {
                                tv_shipping_module_zip_code.text = "-"
                            } else {
                                tv_shipping_module_zip_code.text = invoiceArrayList[i].ship_zipcode
                            }

                            // commission module module
                            if (invoiceArrayList[i].sales_comm_per == "") {
                                tv_comm_module_comm_percent.text =
                                    resources.getString(R.string.sales_comm_percent) + " (-%)"
                            } else {
                                tv_comm_module_comm_percent.text =
                                    resources.getString(R.string.sales_comm_percent) + " (" +
                                            invoiceArrayList[i].sales_comm_per + "%)"
                            }
                            if (invoiceArrayList[i].sales_comm_amount == "") {
                                tv_comm_module_comm_amount.text = "-"
                            } else {
                                tv_comm_module_comm_amount.text =
                                    invoiceArrayList[i].sales_comm_amount
                            }
                            if (invoiceArrayList[i].plus_amount == "") {
                                tv_comm_module_plus_amount.text = "-"
                            } else {
                                val price = invoiceArrayList[i].plus_amount
                                val checkDecimal = price!!.contains(".")
                                if (checkDecimal) {
                                    tv_comm_module_plus_amount.text = stringFormat(price)
                                } else {
                                    tv_comm_module_plus_amount.text = price
                                }
                            }
                            if (invoiceArrayList[i].sales_split_per == "") {
                                tv_comm_module_sales_percent.text =
                                    resources.getString(R.string.sales_split_percent) + " (-%)"
                            } else {
                                tv_comm_module_sales_percent.text =
                                    resources.getString(R.string.sales_split_percent) + " (" + invoiceArrayList[i].sales_split_per + "%)"
                            }
                            if (invoiceArrayList[i].sales_split_amount == "") {
                                tv_comm_module_sales_amount.text = "-"
                            } else {
                                val sales_split_amount = invoiceArrayList[i].sales_split_amount
                                if (sales_split_amount!!.contains(".")) {
                                    tv_comm_module_sales_amount.text = stringFormat(sales_split_amount)
                                } else {
                                    tv_comm_module_sales_amount.text = sales_split_amount
                                }
                            }
                            if (invoiceArrayList[i].sales_comm_total == "") {
                                tv_comm_module_sales_total.text = "-"
                            } else {
                                val sales_comm_total = invoiceArrayList[i].sales_comm_total
                                if (sales_comm_total!!.contains(".")) {
                                    tv_comm_module_sales_total.text = stringFormat(sales_comm_total)
                                } else {
                                    tv_comm_module_sales_total.text = sales_comm_total
                                }
                            }


                        }
                    } else {
                        // do something
                        Toast.makeText(
                            this@InvoiceDetailsActivity,
                            response.body()!!.result,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

        })
    }

    private fun editDateFormat(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d(activity_name, "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }

    private fun stringFormat(value: String): String {
        return String.format("%.2f", value.toFloat())
    }

}
