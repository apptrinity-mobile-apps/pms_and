package com.indobytes.pms

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.indobytes.pms.Adapters.InvoiceAdapter
import com.indobytes.pms.ApiPojo.*
import com.indobytes.pms.Helper.NetworkStatus
import com.indobytes.pms.Helper.SessionManager
import java.util.*
import kotlin.collections.ArrayList

class PaidInvoiceActivity : AppCompatActivity() {

    private lateinit var paidInvoiceToolBar: Toolbar

    private lateinit var ll_transaction_id: LinearLayout
    private lateinit var ll_sales_rep: LinearLayout
    private lateinit var ll_pay_type: LinearLayout
    private lateinit var ll_ac_name: LinearLayout
    private lateinit var ll_ac_no: LinearLayout
    private lateinit var ll_bank_name: LinearLayout
    private lateinit var ll_bank_branch: LinearLayout
    private lateinit var ll_ifsc: LinearLayout
    private lateinit var ll_check_no: LinearLayout
    private lateinit var ll_pay_date: LinearLayout
    private lateinit var ll_comm_total: LinearLayout
    private lateinit var ll_no_internet: LinearLayout

    private lateinit var tv_transaction_id: TextView
    private lateinit var tv_sales_rep: TextView
    private lateinit var tv_pay_type: TextView
    private lateinit var tv_ac_name: TextView
    private lateinit var tv_ac_no: TextView
    private lateinit var tv_bank_name: TextView
    private lateinit var tv_bank_branch: TextView
    private lateinit var tv_ifsc: TextView
    private lateinit var tv_check_no: TextView
    private lateinit var tv_pay_date: TextView
    private lateinit var tv_comm_total: TextView
    private lateinit var tv_no_invoices_found: TextView

    private lateinit var iv_retry_net: ImageView
    private lateinit var iv_paid_invoice_back: ImageView

    private lateinit var rv_paid_invoices: RecyclerView

    private lateinit var mDialog: Dialog

    private lateinit var sessionManager: SessionManager
    private lateinit var userDetails: HashMap<String, String>
    private lateinit var invoiceAdapter: InvoiceAdapter

    private lateinit var invoiceArrayList: ArrayList<InvoiceDetailsResponse>

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paid_invoice)

        sessionManager = SessionManager(this)
        userDetails = sessionManager.userDetails

        init()

        val invoice_list =
            intent.getSerializableExtra("invoice_list") as ArrayList<InvoiceDetailsResponse>
        val transaction_id = intent.getStringExtra("transaction_id")
        val total_amount = intent.getStringExtra("total_amount")
        val payment_type = intent.getStringExtra("payment_type")
        val bank_name = intent.getStringExtra("bank_name")
        val cheque_number = intent.getStringExtra("cheque_number")
        val acholder_name = intent.getStringExtra("acholder_name")
        val ac_number = intent.getStringExtra("ac_number")
        val branch_name = intent.getStringExtra("branch_name")
        val ifsc_code = intent.getStringExtra("ifsc_code")
        val transfer_on = intent.getStringExtra("transfer_on")

        checkInternet()

        ll_sales_rep.visibility = View.GONE

        tv_transaction_id.text = transaction_id
        tv_sales_rep.text = ""
        tv_pay_type.text = payment_type
        tv_ac_name.text = acholder_name
        tv_ac_no.text = ac_number
        tv_bank_name.text = bank_name
        tv_bank_branch.text = branch_name
        tv_ifsc.text = ifsc_code
        tv_check_no.text = cheque_number
        tv_pay_date.text = transfer_on.substring(0, 10)
        tv_comm_total.text = total_amount

        when (payment_type) {
            "Cash" -> {
                ll_ac_name.visibility = View.GONE
                ll_ac_no.visibility = View.GONE
                ll_bank_name.visibility = View.GONE
                ll_bank_branch.visibility = View.GONE
                ll_ifsc.visibility = View.GONE
                ll_check_no.visibility = View.GONE
            }
            "Transfer" -> {
                ll_ac_name.visibility = View.VISIBLE
                ll_ac_no.visibility = View.VISIBLE
                ll_bank_name.visibility = View.VISIBLE
                ll_bank_branch.visibility = View.VISIBLE
                ll_ifsc.visibility = View.VISIBLE
                ll_check_no.visibility = View.GONE
            }
            "Cheque" -> {
                ll_ac_name.visibility = View.GONE
                ll_ac_no.visibility = View.GONE
                ll_bank_branch.visibility = View.GONE
                ll_ifsc.visibility = View.GONE
                ll_bank_name.visibility = View.VISIBLE
                ll_check_no.visibility = View.VISIBLE
            }
        }

        if (invoice_list.size > 0) {
            tv_no_invoices_found.visibility = View.GONE
            rv_paid_invoices.visibility = View.VISIBLE
            invoiceAdapter = InvoiceAdapter(this, invoice_list)
            rv_paid_invoices.adapter = invoiceAdapter
            mDialog.dismiss()
        } else {
            mDialog.dismiss()
            rv_paid_invoices.visibility = View.GONE
            tv_no_invoices_found.visibility = View.VISIBLE
        }

        iv_retry_net.setOnClickListener {
            checkInternet()
        }

        iv_paid_invoice_back.setOnClickListener {
            onBackPressed()
        }

    }

    private fun init() {

        paidInvoiceToolBar = findViewById(R.id.paidInvoiceToolBar)
        iv_paid_invoice_back = paidInvoiceToolBar.findViewById(R.id.iv_paid_invoice_back)

        ll_transaction_id = findViewById(R.id.ll_transaction_id)
        ll_sales_rep = findViewById(R.id.ll_sales_rep)
        ll_pay_type = findViewById(R.id.ll_pay_type)
        ll_ac_name = findViewById(R.id.ll_ac_name)
        ll_ac_no = findViewById(R.id.ll_ac_no)
        ll_bank_name = findViewById(R.id.ll_bank_name)
        ll_bank_branch = findViewById(R.id.ll_bank_branch)
        ll_ifsc = findViewById(R.id.ll_ifsc)
        ll_check_no = findViewById(R.id.ll_check_no)
        ll_pay_date = findViewById(R.id.ll_pay_date)
        ll_comm_total = findViewById(R.id.ll_comm_total)
        ll_no_internet = findViewById(R.id.ll_no_internet)

        tv_transaction_id = findViewById(R.id.tv_transaction_id)
        tv_sales_rep = findViewById(R.id.tv_sales_rep)
        tv_pay_type = findViewById(R.id.tv_pay_type)
        tv_ac_name = findViewById(R.id.tv_ac_name)
        tv_ac_no = findViewById(R.id.tv_ac_no)
        tv_bank_name = findViewById(R.id.tv_bank_name)
        tv_bank_branch = findViewById(R.id.tv_bank_branch)
        tv_ifsc = findViewById(R.id.tv_ifsc)
        tv_check_no = findViewById(R.id.tv_check_no)
        tv_pay_date = findViewById(R.id.tv_pay_date)
        tv_comm_total = findViewById(R.id.tv_comm_total)
        tv_no_invoices_found = findViewById(R.id.tv_no_invoices_found)

        iv_retry_net = findViewById(R.id.iv_retry_net)

        rv_paid_invoices = findViewById(R.id.rv_paid_invoices)

        mDialog = Dialog(this)
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog.setContentView(R.layout.alert_dialog_loading)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog.setCancelable(false)
        mDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    mDialog.dismiss()
                }
                return true
            }
        })

        val layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        rv_paid_invoices.layoutManager = layoutManager
        rv_paid_invoices.setHasFixedSize(true)
        rv_paid_invoices.isNestedScrollingEnabled = true

        invoiceArrayList = ArrayList()
    }

    private fun checkInternet() {
        mDialog.show()
        if (NetworkStatus.checkConnection(this)) {
            mDialog.dismiss()
            ll_no_internet.visibility = View.GONE
            tv_no_invoices_found.visibility = View.GONE
        } else {
            mDialog.dismiss()
            ll_no_internet.visibility = View.VISIBLE
            tv_no_invoices_found.visibility = View.GONE
        }
    }

}
