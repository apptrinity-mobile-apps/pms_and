package com.indobytes.pms.Helper

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.indobytes.pms.LoginActivity
import com.indobytes.pms.NavigationDrawer
import java.util.HashMap

class SessionManager(context: Context) {
    private var pref: SharedPreferences
    private var editor: SharedPreferences.Editor
    private var PRIVATE_MODE = 0
    private var mContext: Context = context

    init {
        pref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    val isLoggedIn: Boolean
        get() = pref.getBoolean(IS_LOGIN, false)

    val userDetails: HashMap<String, String>
        get() {
            val user = HashMap<String, String>()
            user[EXECUTIVE_ID] = pref.getString(EXECUTIVE_ID, "")!!
            user[EXECUTIVE_NAME] = pref.getString(EXECUTIVE_NAME, "")!!
            user[EXECUTIVE_EMAIL] = pref.getString(EXECUTIVE_EMAIL, "")!!
            user[EXECUTIVE_IMAGE] = pref.getString(EXECUTIVE_IMAGE, "")!!
            user[EXECUTIVE_PHONE] = pref.getString(EXECUTIVE_PHONE, "")!!
            user[EXECUTIVE_FAX] = pref.getString(EXECUTIVE_FAX, "")!!
            user[EXECUTIVE_ADDRESS] = pref.getString(EXECUTIVE_ADDRESS, "")!!
            user[EXECUTIVE_COUNTRY] = pref.getString(EXECUTIVE_COUNTRY, "")!!
            user[EXECUTIVE_STATE] = pref.getString(EXECUTIVE_STATE, "")!!
            user[EXECUTIVE_CITY] = pref.getString(EXECUTIVE_CITY, "")!!
            user[EXECUTIVE_ZIPCODE] = pref.getString(EXECUTIVE_ZIPCODE, "")!!
            user[EXECUTIVE_STATUS] = pref.getString(EXECUTIVE_STATUS, "")!!

            return user
        }

    fun createLogin(
        is_login: Boolean,
        executive_id: String,
        executive_name: String,
        executive_email: String,
        executive_image: String,
        executive_phone: String,
        executive_fax: String,
        executive_address: String,
        executive_country: String,
        executive_state: String,
        executive_city: String,
        executive_zipcode: String,
        executive_status: String
    ) {
        editor.putBoolean(IS_LOGIN, is_login)
        editor.putString(EXECUTIVE_ID, executive_id)
        editor.putString(EXECUTIVE_NAME, executive_name)
        editor.putString(EXECUTIVE_EMAIL, executive_email)
        editor.putString(EXECUTIVE_IMAGE, executive_image)
        editor.putString(EXECUTIVE_PHONE, executive_phone)
        editor.putString(EXECUTIVE_FAX, executive_fax)
        editor.putString(EXECUTIVE_ADDRESS, executive_address)
        editor.putString(EXECUTIVE_COUNTRY, executive_country)
        editor.putString(EXECUTIVE_STATE, executive_state)
        editor.putString(EXECUTIVE_CITY, executive_city)
        editor.putString(EXECUTIVE_ZIPCODE, executive_zipcode)
        editor.putString(EXECUTIVE_STATUS, executive_status)

        editor.commit()
    }

    fun clearSession() {
        editor.clear()
        editor.commit()
    }

    companion object {
        var PREF_NAME = "PMS_APP"
        var IS_LOGIN = "IsLoggedIn"
        var EXECUTIVE_ID = "executive_id"
        var EXECUTIVE_NAME = "executive_name"
        var EXECUTIVE_EMAIL = "executive_email"
        var EXECUTIVE_IMAGE = "executive_image"
        var EXECUTIVE_PHONE = "executive_phone"
        var EXECUTIVE_FAX = "executive_fax"
        var EXECUTIVE_ADDRESS = "executive_address"
        var EXECUTIVE_COUNTRY = "executive_country"
        var EXECUTIVE_STATE = "executive_state"
        var EXECUTIVE_CITY = "executive_city"
        var EXECUTIVE_ZIPCODE = "executive_zipcode"
        var EXECUTIVE_STATUS = "executive_status"
    }

}