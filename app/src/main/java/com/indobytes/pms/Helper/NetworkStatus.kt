package com.indobytes.pms.Helper

import android.content.Context
import android.net.ConnectivityManager

object NetworkStatus {
    fun checkConnection(context: Context): Boolean {
        val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connMgr.activeNetworkInfo
        if (activeNetworkInfo != null) {
            // connected to the internet
            return true
        }
        return false
    }
}
