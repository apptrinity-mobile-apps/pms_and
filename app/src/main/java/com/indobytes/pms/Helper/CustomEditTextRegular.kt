package com.indobytes.pms.Helper

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.Gravity
import android.widget.EditText


class CustomEditTextRegular : EditText {

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context) : super(context) {
        init()
    }

    private fun init() {
        if (!isInEditMode) {
            val tf = Typeface.createFromAsset(context.assets, "fonts/Poppins-Regular_1.ttf")
            typeface = tf
        }
    }

    override fun setTypeface(tf: Typeface?, style: Int) {
        var tf = tf
        tf = Typeface.createFromAsset(context.assets, "fonts/Poppins-Regular_1.ttf")
        super.setTypeface(tf, style)
    }

    override fun setTypeface(tf: Typeface?) {
        var tf = tf
        tf = Typeface.createFromAsset(context.assets, "fonts/Poppins-Regular_1.ttf")
        super.setTypeface(tf)
    }


}
