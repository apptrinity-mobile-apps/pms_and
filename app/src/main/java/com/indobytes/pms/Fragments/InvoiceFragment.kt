package com.indobytes.pms.Fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.indobytes.pms.Adapters.InvoiceAdapter
import com.indobytes.pms.Adapters.InvoicePaidAdapter
import com.indobytes.pms.ApiPojo.*
import com.indobytes.pms.Helper.NetworkStatus
import com.indobytes.pms.Helper.SessionManager
import com.indobytes.pms.NavigationDrawer
import com.indobytes.pms.R
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@SuppressLint("SetTextI18n")
class InvoiceFragment : Fragment() {

    private var rootview: View? = null
    private var toolBar: Toolbar? = null
    private var tv_nav_header: TextView? = null
    private var iv_nav_menu: ImageView? = null
    private var rv_invoice_list: RecyclerView? = null
    private lateinit var tv_empty_invoice_details: TextView
    private lateinit var tv_payment_to_date: TextView
    private lateinit var tv_payment_from_date: TextView
    private lateinit var tv_payment_total_amt: TextView
    private lateinit var iv_try_again: ImageView
    private lateinit var ll_total_payments: LinearLayout
    private lateinit var ll_no_network: LinearLayout
    private lateinit var cv_header: CardView
    private lateinit var iv_header_icon: ImageView
    private lateinit var tv_header_amount: TextView
    private lateinit var tv_header_count: TextView
    private lateinit var tv_header_name: TextView

    private lateinit var invoiceAdapter: InvoiceAdapter
    private lateinit var invoicePaidAdapter: InvoicePaidAdapter

    private lateinit var invoiceArrayList: ArrayList<InvoiceDetailsResponse>
    private lateinit var paidInvoiceArrayList: ArrayList<PaidInvoicesDataResponse>

    private lateinit var mDialog: Dialog

    private lateinit var sessionManager: SessionManager
    private lateinit var userDetails: HashMap<String, String>

    private val activity_name = "InvoiceFragment"


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)
        }
        try {
            rootview = inflater.inflate(R.layout.fragment_invoice_detailed, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }

        initialize(rootview)

        return rootview
    }

    private fun initialize(rootView: View?) {

        /*** initialization start ***/
        toolBar = rootView!!.findViewById(R.id.toolBar)
        tv_nav_header = toolBar!!.findViewById(R.id.tv_nav_header)
        iv_nav_menu = toolBar!!.findViewById(R.id.iv_nav_menu)
        rv_invoice_list = rootView.findViewById(R.id.rv_invoice_list)
        tv_empty_invoice_details = rootView.findViewById(R.id.tv_empty_invoice_details)
        ll_no_network = rootView.findViewById(R.id.ll_no_network)
        iv_try_again = rootView.findViewById(R.id.iv_try_again)
        ll_total_payments = rootView.findViewById(R.id.ll_total_payments)
        tv_payment_from_date = rootView.findViewById(R.id.tv_payment_from_date)
        tv_payment_to_date = rootView.findViewById(R.id.tv_payment_to_date)
        tv_payment_total_amt = rootView.findViewById(R.id.tv_payment_total_amt)

        cv_header = rootView.findViewById(R.id.cv_header)
        iv_header_icon = rootView.findViewById(R.id.iv_header_icon)
        tv_header_name = rootView.findViewById(R.id.tv_header_name)
        tv_header_count = rootView.findViewById(R.id.tv_header_count)
        tv_header_amount = rootView.findViewById(R.id.tv_header_amount)

        val layoutManager = LinearLayoutManager(activity!!, LinearLayout.VERTICAL, false)
        rv_invoice_list!!.layoutManager = layoutManager
        rv_invoice_list!!.setHasFixedSize(true)

        invoiceArrayList = ArrayList()

        sessionManager = SessionManager(activity!!)
        userDetails = sessionManager.userDetails

        mDialog = Dialog(activity!!)
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog.setContentView(R.layout.alert_dialog_loading)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog.setCancelable(false)
        mDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    mDialog.dismiss()
                }
                return true
            }
        })

        // disabling navigation drawer
        NavigationDrawer.disableDrawer()

        val header = arguments!!.getString("header")
        if (header == "") {
            tv_nav_header!!.text = resources.getString(R.string.app_name)
        } else {
            tv_nav_header!!.text = header
        }

        /*** initialization end ***/

        checkInternet()

        iv_try_again.setOnClickListener {
            checkInternet()
        }

        iv_nav_menu!!.setOnClickListener {
            // NavigationDrawer.openDrawer()
            val mFragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            mFragmentTransaction.add(R.id.content_frame, DashBoardFragment())
            mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            mFragmentTransaction.addToBackStack(null)

            mFragmentTransaction.commit()
        }
    }

    private fun checkInternet() {
        mDialog.show()
        if (NetworkStatus.checkConnection(activity!!)) {
            getCountsList(userDetails[SessionManager.EXECUTIVE_ID]!!)
            if (tv_nav_header!!.text == resources.getString(R.string.paid_invoices) || tv_nav_header!!.text == resources.getString(
                    R.string.processed_invoices
                )
            ) {
                processedInvoicesList(userDetails[SessionManager.EXECUTIVE_ID]!!)
            } else if (tv_nav_header!!.text == resources.getString(R.string.current_menu_payments)) {
                getPaymentsList(userDetails[SessionManager.EXECUTIVE_ID]!!)
            } else if (tv_nav_header!!.text == resources.getString(R.string.next_menu_payments)) {
                getNextSalePaymentsList(userDetails[SessionManager.EXECUTIVE_ID]!!)
            } else if (tv_nav_header!!.text == resources.getString(R.string.due_menu_payments) || tv_nav_header!!.text == resources.getString(
                    R.string.due_invoices
                )
            ) {
                dueInvoicesList(userDetails[SessionManager.EXECUTIVE_ID]!!)
            } else if (tv_nav_header!!.text == resources.getString(R.string.transaction_invoices)) {
                paidInvoicesList(userDetails[SessionManager.EXECUTIVE_ID]!!)
            } else {
                getInvoiceList(userDetails[SessionManager.EXECUTIVE_ID]!!)
            }
            ll_no_network.visibility = View.GONE
            tv_empty_invoice_details.visibility = View.GONE
        } else {
            mDialog.dismiss()
            ll_no_network.visibility = View.VISIBLE
            tv_empty_invoice_details.visibility = View.GONE
        }
    }

    private fun getInvoiceList(user_id: String) {
        invoiceArrayList = ArrayList()
        val invoiceList = ApiInterface.create()
        var call: Call<InvoiceResponse>? = null

        when (tv_nav_header!!.text) {
            resources.getString(R.string.due_invoices) -> {
                call = invoiceList.dueInvoicesApi(user_id)
            }
            resources.getString(R.string.processed_invoices) -> {
                call = invoiceList.processedInvoicesApi(user_id)
            }
        }

        call!!.enqueue(object : Callback<InvoiceResponse> {
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                Log.d(activity_name, "getInvoiceList error $t")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>, response: Response<InvoiceResponse>
            ) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        val invoice_list = response.body()!!.invoice_list
                        invoiceArrayList = invoice_list!!

                        if (invoiceArrayList.size > 0) {

                            rv_invoice_list!!.visibility = View.VISIBLE
                            tv_empty_invoice_details.visibility = View.GONE
                            invoiceAdapter = InvoiceAdapter(activity!!, invoiceArrayList)
                            rv_invoice_list!!.adapter = invoiceAdapter

                        } else {
                            rv_invoice_list!!.visibility = View.GONE
                            tv_empty_invoice_details.visibility = View.VISIBLE
                        }

                    } else {
                        // do something
                        Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                            .show()
                        rv_invoice_list!!.visibility = View.GONE
                        tv_empty_invoice_details.visibility = View.VISIBLE
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    rv_invoice_list!!.visibility = View.GONE
                    tv_empty_invoice_details.visibility = View.VISIBLE
                }
            }

        })


    }

    private fun paidInvoicesList(user_id: String) {
        paidInvoiceArrayList = ArrayList()
        val invoiceList = ApiInterface.create()
        val paid_call = invoiceList.paidInvoicesApi(user_id)
        paid_call.enqueue(object : Callback<PaidInvoicesResponse> {
            override fun onFailure(call: Call<PaidInvoicesResponse>, t: Throwable) {
                Log.d(activity_name, "paidInvoicesApi error $t")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<PaidInvoicesResponse>, response: Response<PaidInvoicesResponse>
            ) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        val invoice_list = response.body()!!.payment_data
                        paidInvoiceArrayList = invoice_list!!

                        if (paidInvoiceArrayList.size > 0) {

                            rv_invoice_list!!.visibility = View.VISIBLE
                            tv_empty_invoice_details.visibility = View.GONE
                            invoicePaidAdapter =
                                InvoicePaidAdapter(activity!!, paidInvoiceArrayList)
                            rv_invoice_list!!.adapter = invoicePaidAdapter

                        } else {
                            rv_invoice_list!!.visibility = View.GONE
                            tv_empty_invoice_details.visibility = View.VISIBLE
                        }

                    } else {
                        Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                            .show()
                        rv_invoice_list!!.visibility = View.GONE
                        tv_empty_invoice_details.visibility = View.VISIBLE
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    rv_invoice_list!!.visibility = View.GONE
                    tv_empty_invoice_details.visibility = View.VISIBLE
                }
            }

        })
    }

    private fun processedInvoicesList(user_id: String) {
        invoiceArrayList = ArrayList()
        val invoiceList = ApiInterface.create()
        val call: Call<InvoiceResponse>? = invoiceList.processedInvoicesApi(user_id)
        call!!.enqueue(object : Callback<InvoiceResponse> {
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                Log.d(activity_name, "getInvoiceList error $t")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>, response: Response<InvoiceResponse>
            ) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        val invoice_list = response.body()!!.invoice_list
                        invoiceArrayList = invoice_list!!

                        if (invoiceArrayList.size > 0) {

                            rv_invoice_list!!.visibility = View.VISIBLE
                            tv_empty_invoice_details.visibility = View.GONE
                            invoiceAdapter = InvoiceAdapter(activity!!, invoiceArrayList)
                            rv_invoice_list!!.adapter = invoiceAdapter

                        } else {
                            rv_invoice_list!!.visibility = View.GONE
                            tv_empty_invoice_details.visibility = View.VISIBLE
                        }

                    } else {
                        // do something
                        Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                            .show()
                        rv_invoice_list!!.visibility = View.GONE
                        tv_empty_invoice_details.visibility = View.VISIBLE
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    rv_invoice_list!!.visibility = View.GONE
                    tv_empty_invoice_details.visibility = View.VISIBLE
                }
            }

        })


    }

    private fun dueInvoicesList(user_id: String) {
        invoiceArrayList = ArrayList()
        val invoiceList = ApiInterface.create()
        val call: Call<InvoiceResponse>? = invoiceList.dueInvoicesApi(user_id)
        call!!.enqueue(object : Callback<InvoiceResponse> {
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                Log.d(activity_name, "getInvoiceList error $t")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>, response: Response<InvoiceResponse>
            ) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        val invoice_list = response.body()!!.invoice_list
                        invoiceArrayList = invoice_list!!

                        if (invoiceArrayList.size > 0) {

                            rv_invoice_list!!.visibility = View.VISIBLE
                            tv_empty_invoice_details.visibility = View.GONE
                            invoiceAdapter = InvoiceAdapter(activity!!, invoiceArrayList)
                            rv_invoice_list!!.adapter = invoiceAdapter

                        } else {
                            rv_invoice_list!!.visibility = View.GONE
                            tv_empty_invoice_details.visibility = View.VISIBLE
                        }

                    } else {
                        // do something
                        Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                            .show()
                        rv_invoice_list!!.visibility = View.GONE
                        tv_empty_invoice_details.visibility = View.VISIBLE
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    rv_invoice_list!!.visibility = View.GONE
                    tv_empty_invoice_details.visibility = View.VISIBLE
                }
            }

        })


    }

    private fun getPaymentsList(user_id: String) {
        invoiceArrayList = ArrayList()
        val salesPaymentsListApi = ApiInterface.create()
        val call = salesPaymentsListApi.salesPaymentsApi(user_id)
        call.enqueue(object : Callback<InvoiceResponse> {
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                Log.d(activity_name, "salesPaymentsApi error $t")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>, response: Response<InvoiceResponse>
            ) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        val invoice_list = response.body()!!.payment_data
                        invoiceArrayList = invoice_list!!

                        if (response.body()!!.startdate.equals(null) || response.body()!!.startdate == "") {
                            tv_payment_from_date.text = "-"
                        } else {
                            tv_payment_from_date.text =
                                editDateFormat(response.body()!!.startdate!!)
                        }
                        if (response.body()!!.enddate.equals(null) || response.body()!!.enddate == "") {
                            tv_payment_to_date.text = "-"
                        } else {
                            tv_payment_to_date.text = editDateFormat(response.body()!!.enddate!!)
                        }
                        if (response.body()!!.totalamount.equals(null) || response.body()!!.totalamount == "") {
                            tv_payment_total_amt.text = "-"
                        } else {
                            tv_payment_total_amt.text =
                                "$${roundOff(response.body()!!.totalamount!!)}"
                        }

                        if (invoiceArrayList.size > 0) {

                            rv_invoice_list!!.visibility = View.VISIBLE
                            tv_empty_invoice_details.visibility = View.GONE
                            invoiceAdapter = InvoiceAdapter(activity!!, invoiceArrayList)
                            rv_invoice_list!!.adapter = invoiceAdapter

                        } else {
                            rv_invoice_list!!.visibility = View.GONE
                            tv_empty_invoice_details.visibility = View.VISIBLE
                        }
                    } else {
                        Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                            .show()
                        rv_invoice_list!!.visibility = View.GONE
                        tv_empty_invoice_details.visibility = View.VISIBLE
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    rv_invoice_list!!.visibility = View.GONE
                    tv_empty_invoice_details.visibility = View.VISIBLE
                }
            }

        })
    }

    private fun getNextSalePaymentsList(user_id: String) {
        invoiceArrayList = ArrayList()
        val salesPaymentsListApi = ApiInterface.create()
        val call = salesPaymentsListApi.salesNextPaymentsApi(user_id)
        call.enqueue(object : Callback<InvoiceResponse> {
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                Log.d(activity_name, "salesPaymentsApi error $t")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>, response: Response<InvoiceResponse>
            ) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        val invoice_list = response.body()!!.payment_data
                        invoiceArrayList = invoice_list!!

                        if (response.body()!!.startdate.equals(null) || response.body()!!.startdate == "") {
                            tv_payment_from_date.text = "-"
                        } else {
                            tv_payment_from_date.text =
                                editDateFormat(response.body()!!.startdate!!)
                        }
                        if (response.body()!!.enddate.equals(null) || response.body()!!.enddate == "") {
                            tv_payment_to_date.text = "-"
                        } else {
                            tv_payment_to_date.text = response.body()!!.enddate!!
                        }
                        if (response.body()!!.totalamount.equals(null) || response.body()!!.totalamount == "") {
                            tv_payment_total_amt.text = "-"
                        } else {
                            tv_payment_total_amt.text =
                                "$${roundOff(response.body()!!.totalamount!!)}"
                        }

                        if (invoiceArrayList.size > 0) {

                            rv_invoice_list!!.visibility = View.VISIBLE
                            tv_empty_invoice_details.visibility = View.GONE
                            invoiceAdapter = InvoiceAdapter(activity!!, invoiceArrayList)
                            rv_invoice_list!!.adapter = invoiceAdapter

                        } else {
                            rv_invoice_list!!.visibility = View.GONE
                            tv_empty_invoice_details.visibility = View.VISIBLE
                        }
                    } else {
                        Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                            .show()
                        rv_invoice_list!!.visibility = View.GONE
                        tv_empty_invoice_details.visibility = View.VISIBLE
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    rv_invoice_list!!.visibility = View.GONE
                    tv_empty_invoice_details.visibility = View.VISIBLE
                }
            }

        })
    }

    private fun getCountsList(user_id: String) {
        val invoiceList = ApiInterface.create()
        val call = invoiceList.invoicesListApi(user_id)
        call.enqueue(object : Callback<InvoiceResponse> {
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                Log.d(activity_name, "invoicesListApi error $t")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>, response: Response<InvoiceResponse>
            ) {
                Log.d(activity_name, "invoicesListApi success $response")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        if (tv_nav_header!!.text == resources.getString(R.string.current_menu_payments) || tv_nav_header!!.text == resources.getString(
                                R.string.next_menu_payments
                            )
                        ) {
                            ll_total_payments.visibility = View.VISIBLE
                        } else {
                            ll_total_payments.visibility = View.GONE
                        }
                        cv_header.visibility = View.VISIBLE

                        if (tv_nav_header!!.text == resources.getString(R.string.paid_invoices) || tv_nav_header!!.text == resources.getString(
                                R.string.processed_invoices
                            )
                        ) {
                            cv_header.setCardBackgroundColor(
                                ContextCompat.getColor(activity!!, R.color.card_color_process)
                            )
                            iv_header_icon.setImageResource(R.drawable.ic_paid_new)
                            tv_header_name.text = resources.getString(R.string.paid)
                            if (response.body()!!.totalcounts!!.paid_inv_count.equals(null) || response.body()!!.totalcounts!!.paid_inv_count.equals(
                                    ""
                                )
                            ) {
                                tv_header_count.text = "-"
                            } else {
                                tv_header_count.text =
                                    response.body()!!.totalcounts!!.paid_inv_count
                            }
                            if (response.body()!!.totalcounts!!.paid_inv_amount.equals(null) || response.body()!!.totalcounts!!.paid_inv_amount.equals(
                                    ""
                                )
                            ) {
                                tv_header_amount.text = "-"
                            } else {
                                tv_header_amount.text =
                                    "$${roundOff(response.body()!!.totalcounts!!.paid_inv_amount!!)}"
                            }
                        } else if (tv_nav_header!!.text == resources.getString(R.string.current_menu_payments)) {
                            cv_header.setCardBackgroundColor(
                                ContextCompat.getColor(activity!!, R.color.card_color_process)
                            )
                            iv_header_icon.setImageResource(R.drawable.ic_process_new)
                            tv_header_name.text = resources.getString(R.string.current_payments)
                            if (response.body()!!.totalcounts!!.current_payment_count.equals(null) || response.body()!!.totalcounts!!.current_payment_count.equals(
                                    ""
                                )
                            ) {
                                tv_header_count.text = "-"
                            } else {
                                tv_header_count.text =
                                    response.body()!!.totalcounts!!.current_payment_count
                            }
                            if (response.body()!!.totalcounts!!.current_payment_amount.equals(null) || response.body()!!.totalcounts!!.current_payment_amount.equals(
                                    ""
                                )
                            ) {
                                tv_header_amount.text = "-"
                            } else {
                                tv_header_amount.text =
                                    "$${roundOff(response.body()!!.totalcounts!!.current_payment_amount!!)}"
                            }
                        } else if (tv_nav_header!!.text == resources.getString(R.string.next_menu_payments)) {
                            cv_header.setCardBackgroundColor(
                                ContextCompat.getColor(activity!!, R.color.card_color_due)
                            )
                            iv_header_icon.setImageResource(R.drawable.ic_next_new)
                            tv_header_name.text = resources.getString(R.string.next_payments)
                            if (response.body()!!.totalcounts!!.next_payment_count.equals(null) || response.body()!!.totalcounts!!.next_payment_count.equals(
                                    ""
                                )
                            ) {
                                tv_header_count.text = "-"
                            } else {
                                tv_header_count.text =
                                    response.body()!!.totalcounts!!.next_payment_count
                            }
                            if (response.body()!!.totalcounts!!.next_payment_amount.equals(null) || response.body()!!.totalcounts!!.next_payment_amount.equals(
                                    ""
                                )
                            ) {
                                tv_header_amount.text = "-"
                            } else {
                                tv_header_amount.text =
                                    "$${roundOff(response.body()!!.totalcounts!!.next_payment_amount!!)}"
                            }
                        } else if (tv_nav_header!!.text == resources.getString(R.string.due_menu_payments) || tv_nav_header!!.text == resources.getString(
                                R.string.due_invoices
                            )
                        ) {
                            cv_header.setCardBackgroundColor(
                                ContextCompat.getColor(activity!!, R.color.card_color_due)
                            )
                            iv_header_icon.setImageResource(R.drawable.ic_due_new)
                            if (tv_nav_header!!.text == resources.getString(R.string.due_invoices)) {
                                tv_header_name.text = resources.getString(R.string.due_invoices)
                            } else {
                                tv_header_name.text = resources.getString(R.string.due_payments)
                            }
                            if (response.body()!!.totalcounts!!.due_inv_count.equals(null) || response.body()!!.totalcounts!!.due_inv_count.equals(
                                    ""
                                )
                            ) {
                                tv_header_count.text = "-"
                            } else {
                                tv_header_count.text = response.body()!!.totalcounts!!.due_inv_count
                            }
                            if (response.body()!!.totalcounts!!.due_inv_amount.equals(null) || response.body()!!.totalcounts!!.due_inv_amount.equals(
                                    ""
                                )
                            ) {
                                tv_header_amount.text = "-"
                            } else {
                                tv_header_amount.text =
                                    "$${roundOff(response.body()!!.totalcounts!!.due_inv_amount!!)}"
                            }
                        } else if (tv_nav_header!!.text == resources.getString(R.string.transaction_invoices)) {
                            cv_header.visibility = View.GONE
                        }

                    } else {
                        // do something
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun editDateFormat(date: String): String {
        val formattedDate: String
        val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        fromFormat.isLenient = false
        val toFormat = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
        toFormat.isLenient = false
        val date_stg = fromFormat.parse(date)
        formattedDate = toFormat.format(date_stg).toString()
        Log.d(activity_name, "date: $date ======== formatted date $formattedDate")
        return formattedDate
    }

    fun roundOff(value: String?): String {
        val roundedValue: String
        val checkDecimal = value!!.contains(".")
        roundedValue = if (checkDecimal) {
            String.format("%.2f", value.toFloat())
        } else {
            value
        }
        return roundedValue
    }

}
