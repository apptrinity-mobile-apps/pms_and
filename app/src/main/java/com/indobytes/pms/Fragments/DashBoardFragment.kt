package com.indobytes.pms.Fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.*
import com.indobytes.pms.Adapters.InvoiceAdapter
import com.indobytes.pms.ApiPojo.ApiInterface
import com.indobytes.pms.ApiPojo.InvoiceDetailsResponse
import com.indobytes.pms.ApiPojo.InvoiceResponse
import com.indobytes.pms.Helper.NetworkStatus
import com.indobytes.pms.Helper.SessionManager
import com.indobytes.pms.NavigationDrawer
import com.indobytes.pms.R
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DashBoardFragment : Fragment() {

    private var rootview: View? = null
    private lateinit var customToolBar: Toolbar
    private lateinit var iv_retry: ImageView
    private lateinit var iv_nav_bar: ImageView
    private lateinit var cv_due_invoice: CardView
    private lateinit var cv_due_payments_invoice: CardView
    private lateinit var cv_current_payments_invoice: CardView
    private lateinit var cv_next_payments_invoice: CardView
    private lateinit var cv_paid_invoice: CardView
    private lateinit var cv_invoice_filter: CardView
    private lateinit var cv_processed_invoice: CardView
    private lateinit var btn_submit: TextView
    private lateinit var tv_empty_invoice: TextView
    private lateinit var tv_due_invoice_count: TextView
    private lateinit var tv_current_payment_count: TextView
    private lateinit var tv_current_payment_amount: TextView
    private lateinit var tv_next_payments_count: TextView
    private lateinit var tv_next_payments_amount: TextView
    private lateinit var tv_due_payments_count: TextView
    private lateinit var tv_due_payments_amount: TextView
    private lateinit var tv_paid_invoice_count: TextView
    private lateinit var tv_processed_invoice_count: TextView
    private lateinit var tv_due_invoice_amount: TextView
    private lateinit var tv_paid_invoice_amount: TextView
    private lateinit var tv_processed_invoice_amount: TextView
    private lateinit var rv_invoices: RecyclerView
    private lateinit var invoice_filter: Dialog
    private lateinit var et_invoice_no: EditText
    private lateinit var sp_filter_option: Spinner
    private lateinit var ll_no_internet: LinearLayout
    private lateinit var invoiceAdapter: InvoiceAdapter
    private lateinit var filterAdapter: ArrayAdapter<String>

    private lateinit var mDialog: Dialog

    private var selected_filter = ""
    private var inv_type = ""
    private var inv_no = ""
    private val activity_name = "DashBoardFragment"

    private lateinit var sessionManager: SessionManager
    private lateinit var userDetails: HashMap<String, String>

    private lateinit var invoiceArrayList: ArrayList<InvoiceDetailsResponse>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)
        }
        try {
            rootview = inflater.inflate(R.layout.fragment_dash_board, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }

        initialize(rootview)

        return rootview
    }

    private fun initialize(rootView: View?) {
        /*** initialization start ***/
        customToolBar = rootView!!.findViewById(R.id.customToolBar)
        iv_nav_bar = customToolBar.findViewById(R.id.iv_nav_bar)
        cv_invoice_filter = rootView.findViewById(R.id.cv_invoice_filter)
        cv_processed_invoice = rootView.findViewById(R.id.cv_processed_invoice)
        cv_due_invoice = rootView.findViewById(R.id.cv_due_invoice)
        cv_due_payments_invoice = rootView.findViewById(R.id.cv_due_payments_invoice)
        cv_current_payments_invoice = rootView.findViewById(R.id.cv_current_payments_invoice)
        cv_next_payments_invoice = rootView.findViewById(R.id.cv_next_payments_invoice)
        cv_paid_invoice = rootView.findViewById(R.id.cv_paid_invoice)
        tv_paid_invoice_count = rootView.findViewById(R.id.tv_paid_invoice_count)
        tv_processed_invoice_count = rootView.findViewById(R.id.tv_processed_invoice_count)
        tv_due_invoice_count = rootView.findViewById(R.id.tv_due_invoice_count)
        tv_next_payments_count = rootView.findViewById(R.id.tv_next_payments_count)
        tv_next_payments_amount = rootView.findViewById(R.id.tv_next_payments_amount)
        tv_current_payment_count = rootView.findViewById(R.id.tv_current_payment_count)
        tv_current_payment_amount = rootView.findViewById(R.id.tv_current_payment_amount)
        tv_due_payments_amount = rootView.findViewById(R.id.tv_due_payments_amount)
        tv_due_payments_count = rootView.findViewById(R.id.tv_due_payments_count)
        tv_paid_invoice_amount = rootView.findViewById(R.id.tv_paid_invoice_amount)
        tv_processed_invoice_amount = rootView.findViewById(R.id.tv_processed_invoice_amount)
        tv_due_invoice_amount = rootView.findViewById(R.id.tv_due_invoice_amount)
        rv_invoices = rootView.findViewById(R.id.rv_invoices)
        tv_empty_invoice = rootView.findViewById(R.id.tv_empty_invoice)
        iv_retry = rootView.findViewById(R.id.iv_retry)
        ll_no_internet = rootView.findViewById(R.id.ll_no_internet)

        sessionManager = SessionManager(activity!!)
        userDetails = sessionManager.userDetails

        mDialog = Dialog(activity!!)
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog.setContentView(R.layout.alert_dialog_loading)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog.setCancelable(false)
        mDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    mDialog.dismiss()
                }
                return true
            }
        })

        invoiceArrayList = ArrayList()

        val layoutManager = LinearLayoutManager(activity!!, LinearLayout.VERTICAL, false)
        rv_invoices.layoutManager = layoutManager
        rv_invoices.setHasFixedSize(true)

        /*** Filter dialog initialisation - *start* ***/
        invoice_filter = Dialog(context!!)
        invoice_filter.requestWindowFeature(Window.FEATURE_NO_TITLE)
        invoice_filter.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        invoice_filter.setContentView(R.layout.filter_dialog)
        invoice_filter.setCanceledOnTouchOutside(true)
        val window_size = invoice_filter.window
        window_size!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        btn_submit = invoice_filter.findViewById(R.id.btn_submit)
        et_invoice_no = invoice_filter.findViewById(R.id.et_invoice_no)
        sp_filter_option = invoice_filter.findViewById(R.id.sp_filter_option)
        /*** Filter dialog initialisation - *end* ***/

        filterAdapter = object : ArrayAdapter<String>(
            activity!!, R.layout.spinner_item, resources.getStringArray(R.array.filter_list)
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getDropDownView(
                position: Int, convertView: View?, parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view.findViewById<TextView>(R.id.textView)
                // Setting hint text color to gray
                if (position == 0) {
                    tv.setTextColor(Color.GRAY)
                } else {
                    tv.setTextColor(
                        ContextCompat.getColor(activity!!, R.color.item_text_color_default)
                    )
                }
                return view
            }
        }
        sp_filter_option.adapter = filterAdapter

        /*** initialization end ***/

        checkInternet()


        cv_due_invoice.setOnClickListener {

            val bundle = Bundle()
            val fragobj = InvoiceFragment()
            bundle.putString("header", resources.getString(R.string.due_invoices))
            fragobj.arguments = bundle
            changeFragment(fragobj, true)


        }
        cv_paid_invoice.setOnClickListener {

            val bundle = Bundle()
            val fragobj = InvoiceFragment()
            bundle.putString("header", resources.getString(R.string.paid_invoices))
            fragobj.arguments = bundle
            changeFragment(fragobj, true)

        }
        cv_due_payments_invoice.setOnClickListener {

            val bundle = Bundle()
            val fragobj = InvoiceFragment()
            bundle.putString("header", resources.getString(R.string.due_menu_payments))
            fragobj.arguments = bundle
            changeFragment(fragobj, true)
        }
        cv_current_payments_invoice.setOnClickListener {

            val bundle = Bundle()
            val fragobj = InvoiceFragment()
            bundle.putString("header", resources.getString(R.string.current_menu_payments))
            fragobj.arguments = bundle
            changeFragment(fragobj, true)
        }
        cv_next_payments_invoice.setOnClickListener {
            val bundle = Bundle()
            val fragobj = InvoiceFragment()
            bundle.putString("header", resources.getString(R.string.next_menu_payments))
            fragobj.arguments = bundle
            changeFragment(fragobj, true)
        }



        iv_retry.setOnClickListener {
            checkInternet()
        }

        cv_invoice_filter.setOnClickListener {
            invoice_filter.show()
        }

        btn_submit.setOnClickListener {

            if (NetworkStatus.checkConnection(activity!!)) {
                mDialog.show()
                if (selected_filter == "") {
                    inv_type = ""
                } else {
                    when (selected_filter) {
                        resources.getString(R.string.due_invoices) -> {
                            inv_type = "0"
                        }
                        resources.getString(R.string.processed_invoices) -> {
                            inv_type = "1"
                        }
                        resources.getString(R.string.paid_invoices) -> {
                            inv_type = "2"
                        }
                    }
                }

                inv_no = et_invoice_no.text.toString()
                filterList(inv_no, inv_type, userDetails[SessionManager.EXECUTIVE_ID]!!)
                et_invoice_no.setText("")
                sp_filter_option.setSelection(0)
                ll_no_internet.visibility = View.GONE
                tv_empty_invoice.visibility = View.GONE
                invoice_filter.dismiss()
            } else {
                mDialog.dismiss()
                invoice_filter.dismiss()
                ll_no_internet.visibility = View.VISIBLE
                tv_empty_invoice.visibility = View.GONE
            }
        }

        sp_filter_option.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long
            ) {
                selected_filter = parentView.getItemAtPosition(position).toString()
                Log.d(activity_name, "selected_filter $selected_filter")
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {}
        }

        iv_nav_bar.setOnClickListener {
            NavigationDrawer.openDrawer()
        }
    }

    private fun checkInternet() {
        mDialog.show()
        if (NetworkStatus.checkConnection(activity!!)) {
            getInvoiceList(userDetails[SessionManager.EXECUTIVE_ID]!!)
            ll_no_internet.visibility = View.GONE
            tv_empty_invoice.visibility = View.GONE
        } else {
            mDialog.dismiss()
            ll_no_internet.visibility = View.VISIBLE
            tv_empty_invoice.visibility = View.GONE
            Toast.makeText(activity!!, "No internet", Toast.LENGTH_SHORT).show()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun getInvoiceList(user_id: String) {
        invoiceArrayList = ArrayList()
        val invoiceList = ApiInterface.create()
        val call = invoiceList.invoicesListApi(user_id)
        call.enqueue(object : Callback<InvoiceResponse> {
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                Log.d(activity_name, "invoicesListApi error $t")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>, response: Response<InvoiceResponse>
            ) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        val invoice_list = response.body()!!.invoice_list
                        invoiceArrayList = invoice_list!!

                        if (response.body()!!.totalcounts!!.due_inv_count.equals(null) || response.body()!!.totalcounts!!.due_inv_count == "") {
                            tv_due_invoice_count.text = "-"
                            tv_due_payments_count.text = "-"
                        } else {
                            tv_due_invoice_count.text =
                                response.body()!!.totalcounts!!.due_inv_count
                            tv_due_payments_count.text =
                                response.body()!!.totalcounts!!.due_inv_count
                        }
                        if (response.body()!!.totalcounts!!.current_payment_count.equals(null) || response.body()!!.totalcounts!!.current_payment_count == "") {
                            tv_current_payment_count.text = "-"
                        } else {
                            tv_current_payment_count.text =
                                response.body()!!.totalcounts!!.current_payment_count
                        }
                        if (response.body()!!.totalcounts!!.current_payment_amount.equals(null) || response.body()!!.totalcounts!!.current_payment_amount == "") {
                            tv_current_payment_amount.text = "-"
                        } else {
                            tv_current_payment_amount.text =
                                "$${roundOff(response.body()!!.totalcounts!!.current_payment_amount!!)}"
                        }
                        if (response.body()!!.totalcounts!!.next_payment_count.equals(null) || response.body()!!.totalcounts!!.next_payment_count == "") {
                            tv_next_payments_count.text = "-"
                        } else {
                            tv_next_payments_count.text =
                                response.body()!!.totalcounts!!.next_payment_count
                        }
                        if (response.body()!!.totalcounts!!.next_payment_amount.equals(null) || response.body()!!.totalcounts!!.next_payment_amount == "") {
                            tv_next_payments_amount.text = "-"
                        } else {
                            tv_next_payments_amount.text =
                                "$${roundOff(response.body()!!.totalcounts!!.next_payment_amount!!)}"
                        }
                        if (response.body()!!.totalcounts!!.paid_inv_count.equals(null) || response.body()!!.totalcounts!!.paid_inv_count == "") {
                            tv_paid_invoice_count.text = "-"
                        } else {
                            tv_paid_invoice_count.text =
                                response.body()!!.totalcounts!!.paid_inv_count
                        }
                        if (response.body()!!.totalcounts!!.processed_inv_count.equals(null) || response.body()!!.totalcounts!!.processed_inv_count == "") {
                            tv_processed_invoice_count.text = "-"
                        } else {
                            tv_processed_invoice_count.text =
                                response.body()!!.totalcounts!!.processed_inv_count
                        }
                        if (response.body()!!.totalcounts!!.due_inv_amount.equals(null) || response.body()!!.totalcounts!!.due_inv_amount == "") {
                            tv_due_invoice_amount.text = "-"
                            tv_due_payments_amount.text = "-"
                        } else {
                            tv_due_invoice_amount.text =
                                "$${roundOff(response.body()!!.totalcounts!!.due_inv_amount!!)}"
                            tv_due_payments_amount.text =
                                "$${roundOff(response.body()!!.totalcounts!!.due_inv_amount!!)}"
                        }
                        if (response.body()!!.totalcounts!!.paid_inv_amount.equals(null) || response.body()!!.totalcounts!!.paid_inv_amount == "") {
                            tv_paid_invoice_amount.text = "-"
                        } else {
                            tv_paid_invoice_amount.text =
                                "$${roundOff(response.body()!!.totalcounts!!.paid_inv_amount!!)}"
                        }
                        if (response.body()!!.totalcounts!!.processed_inv_amount.equals(null) || response.body()!!.totalcounts!!.processed_inv_amount == "") {
                            tv_processed_invoice_amount.text = "-"
                        } else {
                            tv_processed_invoice_amount.text =
                                "$${roundOff(response.body()!!.totalcounts!!.processed_inv_amount!!)}"
                        }

                    } else {
                        // do something
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()

                }
            }
        })
    }

    private fun filterList(invoice_id: String, invoice_type: String, user_id: String) {
        invoiceArrayList = ArrayList()
        val invoiceFilterList = ApiInterface.create()
        val call = invoiceFilterList.viewInvoiceApi(invoice_id, invoice_type, user_id)
        call.enqueue(object : Callback<InvoiceResponse> {
            override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                Log.d(activity_name, "viewInvoiceApi error $t")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(
                call: Call<InvoiceResponse>, response: Response<InvoiceResponse>
            ) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        val invoice_list = response.body()!!.invoice_details
                        invoiceArrayList = invoice_list!!

                        if (invoiceArrayList.size > 0) {

                            rv_invoices.visibility = View.VISIBLE
                            tv_empty_invoice.visibility = View.GONE
                            invoiceAdapter = InvoiceAdapter(activity!!, invoiceArrayList)
                            rv_invoices.adapter = invoiceAdapter

                        } else {
                            rv_invoices.visibility = View.GONE
                            tv_empty_invoice.visibility = View.VISIBLE
                        }

                    } else {
                        // do something
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    rv_invoices.visibility = View.GONE
                    tv_empty_invoice.visibility = View.VISIBLE
                }
            }

        })
    }

    private fun changeFragment(fragment: Fragment, needToAddBackStack: Boolean) {
        val mFragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        mFragmentTransaction.add(R.id.content_frame, fragment)
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        if (needToAddBackStack)
            mFragmentTransaction.addToBackStack(null)

        mFragmentTransaction.commit()
    }

    fun roundOff(value: String?): String {
        val roundedValue: String
        val checkDecimal = value!!.contains(".")
        roundedValue = if (checkDecimal) {
            String.format("%.2f", value.toFloat())
        } else {
            value
        }
        return roundedValue
    }

}
