package com.indobytes.pms.Fragments

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.indobytes.pms.ApiPojo.ApiInterface
import com.indobytes.pms.ApiPojo.LoginResponse
import com.indobytes.pms.Helper.NetworkStatus
import com.indobytes.pms.Helper.SessionManager
import com.indobytes.pms.NavigationDrawer
import com.indobytes.pms.R
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordFragment : Fragment() {

    private var rootview: View? = null
    private lateinit var passwordToolBar: Toolbar
    private lateinit var iv_password_nav_menu: ImageView
    private lateinit var et_old_password: EditText
    private lateinit var et_new_password: EditText
    private lateinit var et_confirm_password: EditText
    private lateinit var tv_submit: TextView

    private lateinit var mDialog: Dialog

    private lateinit var sessionManager: SessionManager
    private lateinit var userDetails: HashMap<String, String>

    private val activity_name = "ChangePasswordFragment"
    private var old_password = ""
    private var new_password = ""
    private var confirm_password = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)
        }
        try {
            rootview = inflater.inflate(R.layout.fragment_change_password, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }

        initialize(rootview)

        return rootview
    }

    private fun initialize(rootView: View?) {
        passwordToolBar = rootView!!.findViewById(R.id.passwordToolBar)
        iv_password_nav_menu = passwordToolBar.findViewById(R.id.iv_password_nav_menu)
        et_old_password = rootView.findViewById(R.id.et_old_password)
        et_new_password = rootView.findViewById(R.id.et_new_password)
        et_confirm_password = rootView.findViewById(R.id.et_confirm_password)
        tv_submit = rootView.findViewById(R.id.tv_submit)

        sessionManager = SessionManager(activity!!)
        userDetails = sessionManager.userDetails

        mDialog = Dialog(activity!!)
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog.setContentView(R.layout.alert_dialog_loading)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog.setCancelable(false)
        mDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    mDialog.dismiss()
                }
                return true
            }
        })

        /* initialisation END */

        // disabling navigation drawer
        NavigationDrawer.disableDrawer()

        iv_password_nav_menu.setOnClickListener {
//            NavigationDrawer.openDrawer()

            val mFragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            mFragmentTransaction.add(R.id.content_frame, DashBoardFragment())
            mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                mFragmentTransaction.addToBackStack(null)

            mFragmentTransaction.commit()
        }

        et_old_password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val password = et_old_password.text.toString()
                when {
                    password.length > 5 -> {
                        et_old_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                    }
                    password.isEmpty() -> {
                        et_old_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                    }
                    else -> {
                        et_old_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0)
                    }
                }
            }
        })

        et_new_password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val password = et_new_password.text.toString()
                et_confirm_password.setText("")
                when {
                    password.length > 5 -> {
                        et_new_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                    }
                    password.isEmpty() -> {
                        et_new_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                    }
                    else -> {
                        et_new_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0)
                    }
                }
            }
        })

        et_confirm_password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val password = et_confirm_password.text.toString()
                when {
                    password.length > 5 -> {
                        et_confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                    }
                    password.isEmpty() -> {
                        et_confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                    }
                    else -> {
                        et_confirm_password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error, 0)
                    }
                }
            }
        })

        tv_submit.setOnClickListener {
            old_password = et_old_password.text.toString()
            new_password = et_new_password.text.toString()
            confirm_password = et_confirm_password.text.toString()

            Log.d(
                activity_name,
                "old $old_password === new $new_password === confirm $confirm_password"
            )

            if (old_password != "" && new_password != "" && confirm_password != "") {
                if (old_password.length > 5 && new_password.length > 5 && confirm_password.length > 5) {
                    if (new_password != confirm_password) {
                        Toast.makeText(activity, "Password doesn't match!", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        mDialog.show()
                        if (NetworkStatus.checkConnection(activity!!)) {
                            changePassword(
                                old_password,
                                new_password,
                                userDetails[SessionManager.EXECUTIVE_ID]!!
                            )
                        } else {
                            mDialog.dismiss()
                            Toast.makeText(activity!!, "No internet", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            } else {
                Toast.makeText(activity, "Fields are mandatory!", Toast.LENGTH_SHORT).show()
            }

        }

    }

    private fun changePassword(current_password: String, new_password: String, user_id: String) {

        val changePassword = ApiInterface.create()
        val call = changePassword.changePasswordApi(current_password, new_password, user_id)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        Toast.makeText(activity,response.body()!!.result,Toast.LENGTH_SHORT)
                            .show()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

        })

    }




}