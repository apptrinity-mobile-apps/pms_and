package com.indobytes.pms

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.indobytes.pms.ApiPojo.ApiInterface
import com.indobytes.pms.ApiPojo.LoginResponse
import com.indobytes.pms.Helper.NetworkStatus
import com.indobytes.pms.Helper.SessionManager
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    val activity_name = "LoginActivity"
    private lateinit var et_login_user_name: EditText
    private lateinit var et_login_password: EditText

    private lateinit var btn_login: TextView

    private lateinit var mDialog: Dialog

    private var user_name = ""
    private var loginStatus = false
    private var password = ""

    private lateinit var sessionManager: SessionManager
    private lateinit var userDetails: HashMap<String, String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        sessionManager = SessionManager(this)
        userDetails = sessionManager.userDetails

        loginStatus = sessionManager.isLoggedIn

        if (loginStatus) {
            val intent = Intent(this@LoginActivity, NavigationDrawer::class.java)
            startActivity(intent)
            finish()
        }

        init()

        btn_login.setOnClickListener {
            user_name = et_login_user_name.text.toString()
            password = et_login_password.text.toString()
            if (user_name == "" || password == "") {
                Toast.makeText(this, "All fields are required!", Toast.LENGTH_SHORT)
                    .show()
            } else {
                mDialog.show()
                if (NetworkStatus.checkConnection(this)) {
                    login(user_name, password)
                } else {
                    mDialog.dismiss()
                    Toast.makeText(this, "No internet", Toast.LENGTH_SHORT).show()
                }
            }
        }

    }

    private fun init() {
        et_login_user_name = findViewById(R.id.et_login_user_name)
        et_login_password = findViewById(R.id.et_login_password)
        btn_login = findViewById(R.id.btn_login)

        mDialog = Dialog(this)
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog.setContentView(R.layout.alert_dialog_loading)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog.setCancelable(false)
        mDialog.setOnKeyListener(object : DialogInterface.OnKeyListener {
            override fun onKey(arg0: DialogInterface, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    mDialog.dismiss()
                }
                return true
            }
        })

    }

    private fun login(email: String, password: String) {
        mDialog.show()

        var executive_id: String
        var executive_name: String
        var executive_email: String
        var executive_image: String
        var executive_phone: String
        var executive_fax: String
        var executive_address: String
        var executive_country: String
        var executive_state: String
        var executive_city: String
        var executive_zipcode: String
        var executive_status: String

        val login = ApiInterface.create()
        val call = login.loginApi(email, password)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                Log.d(activity_name, "loginApi error $t")
                if (mDialog.isShowing)
                    mDialog.dismiss()
                Toast.makeText(this@LoginActivity, "Please try again!", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if (mDialog.isShowing)
                    mDialog.dismiss()
                try {
                    if (response.body()!!.status == "1") {
                        val executive_info = response.body()!!.executive_info

                        executive_id = if (executive_info!!.executive_id.equals(null)) {
                            "0"
                        } else {
                            executive_info.executive_id!!
                        }
                        executive_name = if (executive_info.executive_name.equals(null)) {
                            "0"
                        } else {
                            executive_info.executive_name!!
                        }
                        executive_email = if (executive_info.executive_email.equals(null)) {
                            "0"
                        } else {
                            executive_info.executive_email!!
                        }
                        executive_image = if (executive_info.executive_image.equals(null)) {
                            "0"
                        } else {
                            executive_info.executive_image!!
                        }
                        executive_phone = if (executive_info.executive_phone.equals(null)) {
                            "0"
                        } else {
                            executive_info.executive_phone!!
                        }
                        executive_fax = if (executive_info.executive_fax.equals(null)) {
                            "0"
                        } else {
                            executive_info.executive_fax!!
                        }
                        executive_address = if (executive_info.executive_address.equals(null)) {
                            "0"
                        } else {
                            executive_info.executive_address!!
                        }
                        executive_country = if (executive_info.executive_country.equals(null)) {
                            "0"
                        } else {
                            executive_info.executive_country!!
                        }
                        executive_state = if (executive_info.executive_state.equals(null)) {
                            "0"
                        } else {
                            executive_info.executive_state!!
                        }
                        executive_city = if (executive_info.executive_city.equals(null)) {
                            "0"
                        } else {
                            executive_info.executive_city!!
                        }
                        executive_zipcode = if (executive_info.executive_zipcode.equals(null)) {
                            "0"
                        } else {
                            executive_info.executive_zipcode!!
                        }
                        executive_status = if (executive_info.executive_status.equals(null)) {
                            "0"
                        } else {
                            executive_info.executive_status!!
                        }

                        sessionManager.createLogin(
                            true,
                            executive_id,
                            executive_name,
                            executive_email,
                            executive_image,
                            executive_phone,
                            executive_fax,
                            executive_address,
                            executive_country,
                            executive_state,
                            executive_city,
                            executive_zipcode,
                            executive_status
                        )

                        val intent = Intent(this@LoginActivity, NavigationDrawer::class.java)
                        startActivity(intent)
                        finish()
                    } else {
// do something
                        Toast.makeText(this@LoginActivity,response.body()!!.result,Toast.LENGTH_SHORT)
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}
