package com.indobytes.pms

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import com.indobytes.pms.Adapters.NavigationAdapter
import com.indobytes.pms.ApiPojo.PhotoBaseUrl
import com.indobytes.pms.Fragments.ChangePasswordFragment
import com.indobytes.pms.Fragments.DashBoardFragment
import com.indobytes.pms.Fragments.InvoiceFragment
import com.indobytes.pms.Helper.RecyclerItemClickListener
import com.indobytes.pms.Helper.SessionManager
import com.squareup.picasso.Picasso


@SuppressLint("StaticFieldLeak")
class NavigationDrawer : AppCompatActivity() {

    private lateinit var navigationAdapter: NavigationAdapter
    private lateinit var rv_nav_list: RecyclerView
    private lateinit var cv_nav_profile_edit: CardView
    private lateinit var iv_nav_profile_image: ImageView
    private lateinit var tv_nav_name: TextView
    private lateinit var tv_nav_email: TextView
    private lateinit var actionBarDrawerToggle: ActionBarDrawerToggle

    private var title: Array<String>? = null
    private var icon: Array<Int>? = null

    private var selected_item = ""
    private val activity_name = "NavigationDrawer"
    private val photo_url = PhotoBaseUrl().PHOTO_URL

    private lateinit var sessionManager: SessionManager
    private lateinit var userDetails: HashMap<String, String>

    private lateinit var mFragmentManager: FragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        /*window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )*/
        setContentView(R.layout.navigation_drawer)

        sessionManager = SessionManager(this)
        userDetails = sessionManager.userDetails

        init()

        if (savedInstanceState == null) {
            val tx = supportFragmentManager.beginTransaction()
            tx.replace(R.id.content_frame, DashBoardFragment())
            tx.commit()
            /*changeFragment(DashBoardFragment(), false)*/
        }

        Picasso.with(this)
            .load(photo_url + userDetails[SessionManager.EXECUTIVE_IMAGE])
            .error(R.drawable.ic_profile_default)
            .into(iv_nav_profile_image)
        iv_nav_profile_image.scaleType = ImageView.ScaleType.FIT_XY

        tv_nav_name.text = userDetails[SessionManager.EXECUTIVE_NAME]
        tv_nav_email.text = userDetails[SessionManager.EXECUTIVE_EMAIL]

        cv_nav_profile_edit.setOnClickListener {
            Toast.makeText(this, "Coming soon!", Toast.LENGTH_SHORT).show()
        }

        rv_nav_list.addOnItemTouchListener(
            RecyclerItemClickListener(this, rv_nav_list,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {

                        selected_item = navigationAdapter.getItem(position)
                        navigation_drawer.closeDrawer(ll_drawer!!)
                        navigationAdapter.setSelected(position)

                        when (selected_item) {
                            resources.getString(R.string.home) -> {
                                /*val tx = supportFragmentManager.beginTransaction()
                                tx.replace(R.id.content_frame, DashBoardFragment())
                                tx.commit()*/
                                changeFragment(DashBoardFragment(), false)
                            }
                            resources.getString(R.string.change_password) -> {
                                changeFragment(ChangePasswordFragment(), true)
                            }
                            resources.getString(R.string.logout) -> {
                                val builder = AlertDialog.Builder(this@NavigationDrawer)
                                builder.setTitle("Alert!")
                                    .setMessage("Are you sure you want to Logout?")
                                    .setPositiveButton("Yes",
                                        object : DialogInterface.OnClickListener {
                                            override fun onClick(
                                                dialog: DialogInterface?, p1: Int
                                            ) {
                                                sessionManager.clearSession()
                                                dialog!!.dismiss()
                                                val i = Intent(
                                                    this@NavigationDrawer, LoginActivity::class.java
                                                )
                                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                                startActivity(i)
                                                finish()
                                            }

                                        })
                                    .setNegativeButton("No",
                                        object : DialogInterface.OnClickListener {
                                            override fun onClick(
                                                dialog: DialogInterface?, p1: Int
                                            ) {
                                                dialog!!.dismiss()
                                            }
                                        })
                                val alertDialog = builder.create()
                                alertDialog.show()

                            }
                            else -> {
                                val bundle = Bundle()
                                val fragobj = InvoiceFragment()
                                bundle.putString("header", selected_item)
                                fragobj.arguments = bundle

                                changeFragment(fragobj, true)
                            }
                        }

                        Log.d(activity_name, "selected item $selected_item == position $position")
                    }
                })
        )

    }

    private fun init() {

        rv_nav_list = findViewById(R.id.rv_nav_list)
        ll_drawer = findViewById(R.id.ll_drawer)
        navigation_drawer = findViewById(R.id.navigation_drawer)
        tv_nav_email = findViewById(R.id.tv_nav_email)
        tv_nav_name = findViewById(R.id.tv_nav_name)
        cv_nav_profile_edit = findViewById(R.id.cv_nav_profile_edit)
        iv_nav_profile_image = findViewById(R.id.iv_nav_profile_image)
        actionBarDrawerToggle =
            ActionBarDrawerToggle(this, navigation_drawer, R.string.app_name, R.string.app_name)

        val layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        rv_nav_list.layoutManager = layoutManager
        rv_nav_list.setHasFixedSize(true)

        navigation_drawer.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()

        title = arrayOf(
            resources.getString(R.string.home),
            resources.getString(R.string.due_invoices),
            resources.getString(R.string.paid_invoices),
            resources.getString(R.string.due_menu_payments),
            resources.getString(R.string.current_menu_payments),
            resources.getString(R.string.next_menu_payments),
            resources.getString(R.string.transaction_invoices),
            resources.getString(R.string.change_password),
            resources.getString(R.string.logout)
        )
        icon = arrayOf(
            R.drawable.ic_arrow_next,
            R.drawable.ic_arrow_next,
            R.drawable.ic_arrow_next,
            R.drawable.ic_arrow_next,
            R.drawable.ic_arrow_next,
            R.drawable.ic_arrow_next,
            R.drawable.ic_arrow_next,
            R.drawable.ic_arrow_next,
            R.drawable.ic_arrow_next
        )

        navigationAdapter = NavigationAdapter(this, title!!, icon!!)
        rv_nav_list.adapter = navigationAdapter
        navigationAdapter.notifyDataSetChanged()

        mFragmentManager = supportFragmentManager
    }


    companion object {
        internal lateinit var navigation_drawer: DrawerLayout
        internal var ll_drawer: LinearLayout? = null

        fun openDrawer() {
            navigation_drawer.openDrawer(ll_drawer!!)
        }

        fun disableDrawer() {
            navigation_drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }
    }

    private fun changeFragment(fragment: Fragment, needToAddBackStack: Boolean) {
        val mFragmentTransaction = mFragmentManager.beginTransaction()
        mFragmentTransaction.add(R.id.content_frame, fragment)
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        if (needToAddBackStack)
            mFragmentTransaction.addToBackStack(null)

        mFragmentTransaction.commit()

    }

    override fun onBackPressed() {

        if (navigation_drawer.isDrawerOpen(ll_drawer!!)) {
            navigation_drawer.closeDrawer(ll_drawer!!)
        } else if (mFragmentManager.backStackEntryCount > 0) {
            mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        } else {
            super.onBackPressed()
        }
    }

}
