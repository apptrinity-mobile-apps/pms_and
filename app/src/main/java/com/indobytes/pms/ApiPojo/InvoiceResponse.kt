package com.indobytes.pms.ApiPojo

class InvoiceResponse {

    val status: String? = null
    val result: String? = null
    val startdate : String? = null
    val enddate: String? = null
    val totalamount: String? = null
    val invoice_list: ArrayList<InvoiceDetailsResponse>? = null
    val invoice_details: ArrayList<InvoiceDetailsResponse>?  = null
    val payment_data: ArrayList<InvoiceDetailsResponse>?  = null
    val totalcounts: InvoiceTotalCountsResponse?  = null

}