package com.indobytes.pms.ApiPojo

class InvoiceTotalCountsResponse {
    val due_inv_count: String? = null
    val due_inv_amount: String? = null
    val processed_inv_count: String? = null
    val processed_inv_amount: String? = null
    val paid_inv_count: String? = null
    val paid_inv_amount: String? = null
    val current_payment_amount: String? = null
    val current_payment_count: String? = null
    val next_payment_amount: String? = null
    val next_payment_count: String? = null

}
