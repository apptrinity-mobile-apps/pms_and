package com.indobytes.pms.ApiPojo

class PaidInvoicesDataResponse {

    val payment_id: String? = null
    val transaction_id: String? = null
    val total_amount: String? = null
    val total_invoices: String? = null
    val payment_type: String? = null
    val bank_name: String? = null
    val cheque_number: String? = null
    val acholder_name: String? = null
    val ac_number: String? = null
    val branch_name: String? = null
    val ifsc_code: String? = null
    val transfer_on: String? = null
    val status: String? = null
    val sales_id: String? = null
    val invoiceids: String? = null
    val invoicelist: ArrayList<InvoiceDetailsResponse>? = null

}
