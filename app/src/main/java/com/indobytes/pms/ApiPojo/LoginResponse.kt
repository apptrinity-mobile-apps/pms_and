package com.indobytes.pms.ApiPojo

class LoginResponse {

    val status: String? = null
    val result: String? = null
    val executive_info: LoginDataResponse? = null

}
