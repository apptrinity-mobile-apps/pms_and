package com.indobytes.pms.ApiPojo

import java.io.Serializable

class InvoiceDetailsResponse : Serializable {

    val unique_id: String? = null
    val invoice_id: String? = null
    val customer_id: String? = null
    val ship_addresslani1: String? = null
    val ship_addresslane2: String? = null
    val ship_state: String? = null
    val ship_zipcode: String? = null
    val ship_contact: String? = null
    val ship_fax: String? = null
    val ship_telephone: String? = null
    val terms_code: String? = null
    val invoice_date: String? = null
    val project_name: String? = null
    val total_price: String? = null
    val invoice_type: String? = null
    val service: String? = null
    val sales_id: String? = null
    val sales_rep: String? = null
    val sales_comm_per: String? = null
    val sales_comm_amount: String? = null
    val plus_amount: String? = null
    val remaining_total_amount: String? = null
    val sales_split_per: String? = null
    val sales_split_amount: String? = null
    val sales_comm_total: String? = null
    val mgr_split_per: String? = null
    val mgr_split_amount: String? = null
    val ugmc_total: String? = null
    val due_date: String? = null
    val po_number: String? = null
    val created_on: String? = null
    val status: String? = null
    val ship_city: String? = null
    val customer_name: String? = null
    val customer_code: String? = null
    val customer_email: String? = null
    val customer_phone: String? = null
    val customer_fax: String? = null
    val customer_addresslane1: String? = null
    val customer_addresslane2: String? = null
    val customer_city: String? = null
    val customer_state: String? = null
    val customer_zipcode: String? = null
    val customer_created_on: String? = null
    val customer_status: String? = null
    val executive_id: String? = null
    val executive_name: String? = null
    val executive_email: String? = null
    val executive_image: String? = null
    val executive_phone: String? = null
    val executive_fax: String? = null
    val executive_password: String? = null
    val executive_address: String? = null
    val executive_country: String? = null
    val executive_state: String? = null
    val executive_city: String? = null
    val executive_zipcode: String? = null
    val executive_comm_percent: String? = null
    val executive_split_percent: String? = null
    val executive_status: String? = null
}