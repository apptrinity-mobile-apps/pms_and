package com.indobytes.pms.ApiPojo

class PaidInvoicesResponse {

    val status: String? = null
    val result: String? = null
    val payment_data: ArrayList<PaidInvoicesDataResponse>? = null

}
