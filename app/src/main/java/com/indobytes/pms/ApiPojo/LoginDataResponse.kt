package com.indobytes.pms.ApiPojo

class LoginDataResponse {

    val executive_id: String? = null
    val executive_name: String? = null
    val executive_email: String? = null
    val executive_image: String? = null
    val executive_phone: String? = null
    val executive_fax: String? = null
    val executive_password: String? = null
    val executive_address: String? = null
    val executive_country: String? = null
    val executive_state: String? = null
    val executive_city: String? = null
    val executive_zipcode: String? = null
    val executive_status: String? = null
    val created_on: String? = null

}
