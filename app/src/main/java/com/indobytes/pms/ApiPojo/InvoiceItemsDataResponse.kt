package com.indobytes.pms.ApiPojo

class InvoiceItemsDataResponse {

    val item_id: String? = null
    val invoice_id: String? = null
    val unique_invoice_id: String? = null
    val tax_schedule: String? = null
    val line_item_code: String? = null
    val qty: String? = null
    val unit_price: String? = null
    val ship_method: String? = null
    val item_total_price: String? = null
    val line_item_code_text: String? = null
    val tax_code: String? = null
    val line_tax: String? = null
    val service_code: String? = null
    val service_description: String? = null

}
