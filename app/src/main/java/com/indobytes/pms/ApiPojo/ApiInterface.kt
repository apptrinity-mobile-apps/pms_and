package com.indobytes.pms.ApiPojo

import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface ApiInterface {

    @FormUrlEncoded
    @POST("login")
    fun loginApi(
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<LoginResponse>

    @FormUrlEncoded
    @POST("executiveProfile")
    fun executiveProfileApi(
        @Field("executive_id") executive_id: String
    ): Call<LoginResponse>

    @FormUrlEncoded
    @POST("invoicesList")
    fun invoicesListApi(
        @Field("executive_id") executive_id: String
    ): Call<InvoiceResponse>

    @FormUrlEncoded
    @POST("dueInvoices")
    fun dueInvoicesApi(
        @Field("executive_id") executive_id: String
    ): Call<InvoiceResponse>

    @FormUrlEncoded
    @POST("processedInvoices")
    fun processedInvoicesApi(
        @Field("executive_id") executive_id: String
    ): Call<InvoiceResponse>

    @FormUrlEncoded
    @POST("paidInvoices")
    fun paidInvoicesApi(
        @Field("executive_id") executive_id: String
    ): Call<PaidInvoicesResponse>

    @FormUrlEncoded
    @POST("viewInvoice")
    fun viewInvoiceApi(
        @Field("invoice_id") invoice_id: String,
        @Field("invoice_type") Invoice_type: String,
        @Field("executive_id") executive_id: String
    ): Call<InvoiceResponse>

    @FormUrlEncoded
    @POST("invoiceItems")
    fun invoiceItemsApi(
        @Field("invoice_id") invoice_id: String,
        @Field("executive_id") executive_id: String
    ): Call<InvoiceItemsResponse>

    @FormUrlEncoded
    @POST("changePassword")
    fun changePasswordApi(
        @Field("old_password") old_password: String,
        @Field("new_password") new_password: String,
        @Field("executive_id") executive_id: String
    ): Call<LoginResponse>

    @FormUrlEncoded
    @POST("salesPayments")
    fun salesPaymentsApi(
        @Field("executive_id") executive_id: String
    ): Call<InvoiceResponse>

    @FormUrlEncoded
    @POST("salesNextPayments")
    fun salesNextPaymentsApi(
        @Field("executive_id") executive_id: String
    ): Call<InvoiceResponse>

    companion object Factory {

        private val BASE_URL = "https://4print.com/pms/api/"

        private val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)!!

        fun create(): ApiInterface {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}