package com.indobytes.pms.ApiPojo

class InvoiceItemsResponse {

    val status: String? = null
    val result: String? = null
    val invoice_items: ArrayList<InvoiceItemsDataResponse>? = null

}
